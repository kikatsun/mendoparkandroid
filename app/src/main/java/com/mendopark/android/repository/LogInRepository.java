package com.mendopark.android.repository;

import androidx.lifecycle.LiveData;

import com.mendopark.android.datasource.LogInDataSource;
import com.mendopark.android.dto.MendoParkSessionDto;

public class LogInRepository {

    private static volatile LogInRepository instance;

    private LogInDataSource logInDataSource;

    private LogInRepository (LogInDataSource logInDataSource){
        this.logInDataSource = logInDataSource;
    }

    public static LogInRepository getInstance (LogInDataSource dataSource) {
        if (instance == null) {
            instance = new LogInRepository(dataSource);
        }
        return instance;
    }

    public LiveData<MendoParkSessionDto> createSession (MendoParkSessionDto sessionDto) {
        return logInDataSource.createSession(sessionDto);
    }

}
