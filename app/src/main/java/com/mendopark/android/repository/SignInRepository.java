package com.mendopark.android.repository;

import androidx.lifecycle.LiveData;

import com.mendopark.android.datasource.rest.SignInDataRest;
import com.mendopark.android.dto.UserDto;

public class SignInRepository {

    private static volatile SignInRepository instance;

    private SignInDataRest signInDataRest;

    private SignInRepository(SignInDataRest signInDataRest){
        this.signInDataRest = signInDataRest;
    }

    public static SignInRepository getInstance(SignInDataRest dataSource){
        if(instance == null){
            instance = new SignInRepository(dataSource);
        }
        return instance;
    }

    public LiveData<UserDto> signIn(UserDto newUser){
        return signInDataRest.signIn(newUser);
    }

}
