package com.mendopark.android.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.datasource.rest.ReserveDataRest;
import com.mendopark.android.dto.reserve.ReservaDto;

public class ReserveRepository {

    private static volatile ReserveRepository instance;

    private ReserveDataRest reserveDataRest;

    private ReserveRepository (ReserveDataRest reserveDataRest){
        this.reserveDataRest = reserveDataRest;
    }

    public static ReserveRepository getInstance (ReserveDataRest reserveDataRest) {
        if (instance == null) {
            instance = new ReserveRepository(reserveDataRest);
        }
        return instance;
    }

    public MutableLiveData<ReservaDto> reserveAction(ReservaDto reservaDto){
        return reserveDataRest.reserveAction(reservaDto);
    }

    public LiveData<ReservaDto> cancelAction(ReservaDto reserva) {
        return reserveDataRest.cancelAction(reserva);
    }
}
