package com.mendopark.android.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.dto.ParkingModel;
import com.mendopark.android.dto.find.GeoPoint;
import com.mendopark.android.dto.find.GeoPointConsulted;
import com.mendopark.android.dto.find.ParkingAreaDto;
import com.mendopark.android.dto.find.SensorStateDto;
import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.dto.plate.VehicleDto;
import com.mendopark.android.dto.plate.VehicleTypeDto;
import com.mendopark.android.dto.reserve.ReservaDto;

import java.util.ArrayList;
import java.util.List;

public class FindLocationRepository {

    private static volatile FindLocationRepository instance;

    private FindLocationDataRest findLocationDataRest;

    private FindLocationRepository(FindLocationDataRest findLocationDataRest){
        this.findLocationDataRest = findLocationDataRest;
    }

    public static FindLocationRepository getInstance(FindLocationDataRest dataRest){
        if (instance == null) {
            instance = new FindLocationRepository(dataRest);
        }
        return instance;
    }

    public MutableLiveData<GeoPointConsulted> findLocation(String location){
        return findLocationDataRest.findLocation(location);
    }

    public MutableLiveData<List<ParkingAreaDto>> findParkingAreas(GeoPoint point){
        return findLocationDataRest.findParkingAreas(point);
    }

    public MutableLiveData<SensorStateDto> findParkingSpaceState(String parkingSpaceId){
        return findLocationDataRest.findParkingSpaceState(parkingSpaceId);
    }

    public MutableLiveData<List<PayTypeDto>> findPayTypes() {
        return findLocationDataRest.findPayTypes();
    }

    public MutableLiveData<List<ParkingModel>> findParking() {
        return findLocationDataRest.findParking();
    }

    public MutableLiveData<List<VehicleDto>> findVehicles() {
//        VehicleDto dto1 = new VehicleDto();
//        dto1.setDomain("AAA222");
//        dto1.setModel("Fiesta");
//        dto1.setBrand("Ford");
//        dto1.setYear(2018);
//        dto1.setType(1);
//        VehicleDto dto2 = new VehicleDto();
//        dto2.setDomain("wer345");
//        dto2.setModel("Fiesta");
//        dto2.setBrand("Focus");
//        dto2.setYear(2005);
//        dto2.setType(2);
//        List<VehicleDto> dtos = new ArrayList<>();
//        dtos.add(dto1);
//        dtos.add(dto2);
//        final MutableLiveData<List<VehicleDto>> result = new MutableLiveData<>();
//        result.setValue(dtos);
//        return result;
        return findLocationDataRest.findVechicles();
    }

    public MutableLiveData<List<VehicleTypeDto>> findVehiclesTypes() {
//        VehicleTypeDto typ1 = new VehicleTypeDto();
//        typ1.setName("Tipo1");
//        typ1.setNumber(1);
//        VehicleTypeDto typ2 = new VehicleTypeDto();
//        typ2.setName("Tipo2");
//        typ2.setNumber(2);
//        List<VehicleTypeDto> ty = new ArrayList<>();
//        ty.add(typ1);
//        ty.add(typ2);
//        final MutableLiveData<List<VehicleTypeDto>> types = new MutableLiveData<>();
//        types.setValue(ty);
//        return types;
        return findLocationDataRest.findVehiclesTypes();
    }

    public MutableLiveData<VehicleDto> savePLate(VehicleDto value) {
        return findLocationDataRest.savePlate(value);
    }

    public MutableLiveData<VehicleDto> eliminarPLate(VehicleDto value) {
        return findLocationDataRest.eliminarPlate(value);
    }
}

