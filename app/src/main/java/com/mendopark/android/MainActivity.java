package com.mendopark.android;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.mendopark.android.dto.reserve.ReservaDto;
import com.mendopark.android.model.Role;
import com.mendopark.android.service.Preferencias;
import com.mendopark.android.service.reservation.ReservationManager;
import com.mendopark.android.ui.findlocation.FindLocationFragment;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Preferencias.getInstance(getApplicationContext());

        int roleId = Preferencias.getInstanciaSinContexto().getUserRole();
        if (roleId == Role.COMMON_USER.getRoleId()){
            Intent intent = new Intent(getApplicationContext(), ActivityMainUser.class);
            startActivity(intent);
        }else if (roleId == Role.OPERATIONAL_USER.getRoleId() ||
            roleId == Role.CLIENT_ADMIN_USER.getRoleId() ||
            roleId == Role.SUPERVISOR_USER.getRoleId()){
            Intent intent = new Intent(getApplicationContext(), MainActivityAdmin.class);
            startActivity(intent);
        }else {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            NavigationView navigationView = findViewById(R.id.nav_view);
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.nav_home, R.id.nav_find, R.id.nav_log_in,
                    R.id.nav_sign_in)
                    .setDrawerLayout(drawer)
                    .build();

            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.nav_home);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        try{
            MenuItem menuItem = menu.getItem(0);
            if(menuItem != null){
                menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        cerrar();
                        return false;
                    }
                });
            }
        }catch (Exception e){

        }
        return true;
    }

    private void cerrar() {
        Preferencias.getInstanciaSinContexto().cerrarSession();
        Toast.makeText(
                getApplicationContext(),
                "Sesión finalizada.\n" +
                        "Gracias por utilizar la aplicación",
                Toast.LENGTH_SHORT
        ).show();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

}
