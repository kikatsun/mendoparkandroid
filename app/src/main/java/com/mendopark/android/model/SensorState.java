package com.mendopark.android.model;

public class SensorState {

    public static class State{
        public static final String FREE = "LIBRE";
        public static final String BUSSY = "OCUPADO";
        public static final String RESERVED = "RESERVADO";
        public static final String STATELESS = "SIN_ESTADO";
    }
}
