package com.mendopark.android.model;

public enum ReserveState {

    CREATED(1),
    ACEPTED(2),
    REJECTED(3),
    CANCELLED(4),
    FINISHED(5);

    private int stateId;

    private ReserveState(int stateId){
        this.stateId = stateId;
    }

    public int getStateId(){
        return this.stateId;
    }
}
