package com.mendopark.android.model;

public enum Role {

    CLIENT_ADMIN_USER(2),
    COMMON_USER(3),
    SUPERVISOR_USER(4),
    OPERATIONAL_USER(5);

    private int roleId;

    private Role(int roleId){
        this.roleId = roleId;
    }

    public int getRoleId(){
        return this.roleId;
    }
}
