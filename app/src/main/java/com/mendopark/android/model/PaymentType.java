package com.mendopark.android.model;

public class PaymentType {

    public static final Long CASH = 1L;
    public static final Long CREDIT_CARD = 2L;
    public static final Long DEBIT_CARD = 3L;

}
