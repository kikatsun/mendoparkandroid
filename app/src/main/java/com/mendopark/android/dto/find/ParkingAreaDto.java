package com.mendopark.android.dto.find;

import java.util.ArrayList;
import java.util.List;

public class ParkingAreaDto {

    private Long number;
    private String name;
    private String color;
    private List<PolygonPointsDto> polygonPoints = new ArrayList<PolygonPointsDto>();
    private List<SensorDto> sensors = new ArrayList<SensorDto>();
    private PolygonPointsDto centerPoint;
    private List<ParkingSpaceDto> parkingSpaces;

    private String disponibilidad;
    private Float precio;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<PolygonPointsDto> getPolygonPoints() {
        return polygonPoints;
    }

    public void setPolygonPoints(List<PolygonPointsDto> polygonPoints) {
        this.polygonPoints = polygonPoints;
    }

    public List<SensorDto> getSensors() {
        return sensors;
    }

    public void setSensors(List<SensorDto> sensors) {
        this.sensors = sensors;
    }

    public PolygonPointsDto getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(PolygonPointsDto centerPoint) {
        this.centerPoint = centerPoint;
    }

    public List<ParkingSpaceDto> getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(List<ParkingSpaceDto> parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public String getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(String disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

}
