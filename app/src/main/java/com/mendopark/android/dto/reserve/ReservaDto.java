package com.mendopark.android.dto.reserve;

import com.mendopark.android.dto.payment.PagoDto;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class ReservaDto {

    private String id;
    private Long parkingAreaNumber;
    private String parkingAreaName;
    private String idPlaza;
    private String parkingSpaceName;
    private float priceByHour = 50f;
    private float anticipatedAmount = 20f;
    private String plate;
    private String observations;
    private Long creationDate;

    private String userCreatorId;
    private String eventoAccion;
    private int stateId;
    private String stateObservation;

    private PagoDto pago;

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();

        json.accumulate("parkingAreaNumber",parkingAreaNumber);
        json.accumulate("parkingAreaName",parkingAreaName);
        json.accumulate("idPlaza",idPlaza);
        json.accumulate("parkingSpaceName",parkingSpaceName);
        json.accumulate("priceByHour",priceByHour);
        json.accumulate("anticipatedAmount",anticipatedAmount);
        json.accumulate("plate",plate);
        json.accumulate("observations",observations);
        json.accumulate("creationDate",creationDate);

        return json;
    }

    public String getEventoAccion() {
        return eventoAccion;
    }

    public void setEventoAccion(String eventoAccion) {
        this.eventoAccion = eventoAccion;
    }

    public String getParkingSpaceName() {
        return parkingSpaceName;
    }

    public void setParkingSpaceName(String parkingSpaceName) {
        this.parkingSpaceName = parkingSpaceName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getParkingAreaNumber() {
        return parkingAreaNumber;
    }

    public void setParkingAreaNumber(Long parkingAreaNumber) {
        this.parkingAreaNumber = parkingAreaNumber;
    }

    public String getIdPlaza() {
        return idPlaza;
    }

    public void setIdPlaza(String idPlaza) {
        this.idPlaza = idPlaza;
    }

    public String getParkingAreaName() {
        return parkingAreaName;
    }

    public void setParkingAreaName(String parkingAreaName) {
        this.parkingAreaName = parkingAreaName;
    }

    public float getPriceByHour() {
        return priceByHour;
    }

    public void setPriceByHour(float priceByHour) {
        this.priceByHour = priceByHour;
    }

    public float getAnticipatedAmount() {
        return anticipatedAmount;
    }

    public void setAnticipatedAmount(float anticipatedAmount) {
        this.anticipatedAmount = anticipatedAmount;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public String getUserCreatorId() {
        return userCreatorId;
    }

    public void setUserCreatorId(String userCreatorId) {
        this.userCreatorId = userCreatorId;
    }

    public PagoDto getPago() {
        return pago;
    }

    public void setPago(PagoDto pago) {
        this.pago = pago;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateObservation() {
        return stateObservation;
    }

    public void setStateObservation(String stateObservation) {
        this.stateObservation = stateObservation;
    }
}
