package com.mendopark.android.dto;

import com.google.gson.annotations.SerializedName;
import com.mendopark.android.dto.reserve.ReservaDto;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class ParkingModel  implements Serializable {


    private UUID id;
    private Date start;
    private Date end;
    private int hours;
    private float price;
    private String parkingSpaceName;
    private long parkingAreaNumber;

    private ReservaDto reserva;

    public ReservaDto getReserva() {
        return reserva;
    }

    public void setReserva(ReservaDto reserva) {
        this.reserva = reserva;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getParkingSpaceName() {
        return parkingSpaceName;
    }

    public void setParkingSpaceName(String parkingSpaceName) {
        this.parkingSpaceName = parkingSpaceName;
    }

    public long getParkingAreaNumber() {
        return parkingAreaNumber;
    }

    public void setParkingAreaNumber(long parkingAreaNumber) {
        this.parkingAreaNumber = parkingAreaNumber;
    }
}