package com.mendopark.android.dto.find;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class GeoPointConsulted {

    @SerializedName("display_name")
    @Expose
    private String title;

    @SerializedName("lat")
    @Expose
    private Double latitude;

    @SerializedName("lon")
    @Expose
    private Double longitude;

    public GeoPointConsulted(String title, Double latitude, Double longitude) {
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public GeoPointConsulted(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JSONObject getJSON(){
        JSONObject json = new JSONObject();
        try {
            json.accumulate("latitude", this.latitude);
            json.accumulate("longitude", longitude);
        }catch (JSONException ex){

        }
        return json;
    }

    @NonNull
    @Override
    public String toString() {
        return "title: "+this.title + " ,latitude: "+latitude+" ,longitude: "+longitude;
    }
}
