package com.mendopark.android.dto.find;

public class ParkingSpaceDto {

    private String id;
    private String name;
    private Long fromParkingArea;
    private SensorDto sensor;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFromParkingArea() {
        return fromParkingArea;
    }

    public void setFromParkingArea(Long fromParkingArea) {
        this.fromParkingArea = fromParkingArea;
    }

    public SensorDto getSensor() {
        return sensor;
    }

    public void setSensor(SensorDto sensor) {
        this.sensor = sensor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        id = id;
    }
}
