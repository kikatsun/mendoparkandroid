package com.mendopark.android.dto.payment;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class TarjetDto {

    private String cardNumber;
    private String expiry;
    private String cardHolderName;
    private String cvv;

    public TarjetDto() {

    }

    public TarjetDto (String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.has("cardNumber")){
                cardNumber = jsonObject.getString("cardNumber");
            }
            if (jsonObject.has("expiry")){
                expiry = jsonObject.getString("expiry");
            }
            if (jsonObject.has("cardHolderName")){
                cardHolderName = jsonObject.getString("cardHolderName");
            }
            if (jsonObject.has("cvv")){
                cvv = jsonObject.getString("cvv");
            }

        } catch (JSONException e) {

        }
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    @NonNull
    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        try {
            json.accumulate("cardNumber", cardNumber);
            json.accumulate("expiry", expiry);
            json.accumulate("cardHolderName", cardHolderName);
            json.accumulate("cvv", cvv);
        }catch (JSONException ex){

        }
        return json.toString();
    }

}
