package com.mendopark.android.log;

public class LogName {

    private static final String ERROR = "_Error";

    public static final String getError(Class clazz){
        return clazz.getSimpleName().concat(ERROR);
    }

    public static final String getLogName(Class clazz){
        return clazz.getSimpleName();
    }

}
