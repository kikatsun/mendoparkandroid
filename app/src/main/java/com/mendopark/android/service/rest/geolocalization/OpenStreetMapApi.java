package com.mendopark.android.service.rest.geolocalization;

import com.mendopark.android.dto.find.GeoPoint;
import com.mendopark.android.dto.find.GeoPointConsulted;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface OpenStreetMapApi {

    @GET
    Call<List<GeoPointConsulted>> findPosition(@Url String url);

}
