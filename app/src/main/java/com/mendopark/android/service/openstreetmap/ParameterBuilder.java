package com.mendopark.android.service.openstreetmap;

public class ParameterBuilder {

    private static final String QUERY_PARAMETER_START = "search/?q=";
    private static final String QUERY_PARAMETER_END = "&format=json";

    public static class Field{
        public static final String LATITUDE = "lat";
        public static final String LONGITUDE= "lon";
        public static final String DISPLAY_NAME= "display_name";
    }

    public static final String generateUrlQuery(String location){
        location = location.trim().replaceAll(" ","%20");
        return QUERY_PARAMETER_START+location+QUERY_PARAMETER_END;
    }

}
