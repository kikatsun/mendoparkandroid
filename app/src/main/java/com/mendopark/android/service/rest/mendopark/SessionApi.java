package com.mendopark.android.service.rest.mendopark;

import com.mendopark.android.dto.MendoParkSessionDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SessionApi {

    @POST("session")
    Call<MendoParkSessionDto> createSession(@Body MendoParkSessionDto sessionDto);

}
