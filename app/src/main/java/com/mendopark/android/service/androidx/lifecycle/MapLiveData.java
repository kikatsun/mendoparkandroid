package com.mendopark.android.service.androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import java.util.Map;

public class MapLiveData<M extends Map> extends LiveData<M> {

    @Override
    public void postValue(M value) {
        super.postValue(value);
    }

    @Override
    public void setValue(M value) {
        super.setValue(value);
    }

    public void postValue(String key, Object value){
        M thisValue = getValue();
        thisValue.put(key,value);
        postValue(thisValue);
    }

    public void setValue(String key, Object value) {
        M thisValue = getValue();
        thisValue.put(key, value);
        setValue(thisValue);
    }

    public void postValue(Integer key, Object value){
        M thisValue = getValue();
        thisValue.put(key,value);
        postValue(thisValue);
    }

    public void setValue(Integer key, Object value) {
        M thisValue = getValue();
        thisValue.put(key, value);
        setValue(thisValue);
    }
}
