package com.mendopark.android.service.rest.gitlab;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MiIpAPI {

    @GET("gterickgt/primero/master/primero.txt")
    Call<String> getMiIp();

}
