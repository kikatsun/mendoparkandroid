package com.mendopark.android.service;

import java.util.HashMap;
import java.util.Map;

public class ResponseFactory {

    public static ResponseFactory instance = new ResponseFactory();

    private Map<String, String> response = new HashMap<>();

    private ResponseFactory(){}

    public static ResponseFactory getInstance(){
        if(instance == null){
            instance = new ResponseFactory();
        }
        return instance;
    }

    public void addMessage (String key, String message){
        response.put(key,message);
    }

    public String getMEssage(String key){
        return response.remove(key);
    }

}
