package com.mendopark.android.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mendopark.android.dto.payment.TarjetDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.retrofit.ApiBuilder;

import java.util.HashSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Preferencias {

    private static Preferencias ourInstance = null;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    private final String app = "MendoparkApp";

    private static final String SESSION_ID = "sessionId";
    private static final String IP = "ip";
    private static final String CREDIT_TARGETS = "creditTrajets2";
    private static final String ROL_USER= "rol";

    public static Preferencias getInstance(Context context) {

        if(ourInstance==null){
            ourInstance = new Preferencias(context);
            return ourInstance;
        }
        else{
            return ourInstance;
        }
    }

    public static Preferencias getInstanciaSinContexto(){
        return ourInstance;
    }

    private Preferencias(Context activity){
        sharedPreferences = activity.getSharedPreferences(
                app,
                activity.getApplicationContext().MODE_PRIVATE
        );
        findIp();

    }

    public String getIp(){
        return sharedPreferences.getString(IP, null);
    }

    public void setSessionId(String sessionId){
        editor = sharedPreferences.edit();
        editor.putString(SESSION_ID, sessionId);
        editor.apply();
    }

    public void updateUserRol(int roleId) {
        editor = sharedPreferences.edit();
        editor.putInt(ROL_USER, roleId);
        editor.apply();
    }

    public int getUserRole () {
        return sharedPreferences.getInt(ROL_USER, 0);
    }

    public void addCreditTarjet (TarjetDto tarjet, int i) {
        editor = sharedPreferences.edit();
        editor.putString(String.valueOf(i)+CREDIT_TARGETS, tarjet.toString());
        editor.apply();
        setCreditsNumber(i);
    }

    public HashSet<String> getCreditTarjets (int i) {
        HashSet<String> res = new HashSet<>();
        i++;
        for(int j=1; j<i ; j++){
            String name = String.valueOf(j).concat(CREDIT_TARGETS);
            res.add(sharedPreferences.getString(name, null));
        }
        return res;
    }

    public void setCreditsNumber(int i){
        editor = sharedPreferences.edit();
        editor.putInt("number"+CREDIT_TARGETS,i);
        editor.apply();
    }

    public int getCreditsNumber(){
        return sharedPreferences.getInt("number"+CREDIT_TARGETS, 0);
    }

//    public HashSet<String> clearTarjets () {
//        editor = sharedPreferences.edit();
//        editor.remove(CREDIT_TARGETS);
//        editor.putStringSet(CREDIT_TARGETS, new HashSet<String>());
//        editor.apply();
//        return result;
//    }

    public String getSessionId(){
        return sharedPreferences.getString(SESSION_ID, null);
    }

    private void findIp(){
        ApiBuilder.getMiIp().getMiIp().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.body() != null && !response.body().isEmpty()) {
                    editor = sharedPreferences.edit();
                    editor.putString(IP, response.body());
                    editor.apply();
                }
                Log.i(LogName.getLogName(Preferencias.class), "My ip: "+response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(LogName.getError(Preferencias.class),"Could not find my ip.",t);
            }
        });
    }

    public void cerrarSession() {
        editor = sharedPreferences.edit();
        editor.remove(SESSION_ID);
        editor.remove(ROL_USER);
        editor.apply();
    }
}