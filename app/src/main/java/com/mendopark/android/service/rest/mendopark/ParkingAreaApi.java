package com.mendopark.android.service.rest.mendopark;

import com.mendopark.android.dto.ParkingModel;
import com.mendopark.android.dto.find.ParkingAreaDto;
import com.mendopark.android.dto.find.Point;
import com.mendopark.android.dto.find.SensorStateDto;
import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.dto.plate.VehicleDto;
import com.mendopark.android.dto.plate.VehicleTypeDto;
import com.mendopark.android.dto.reserve.ReservaDto;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface ParkingAreaApi {

    @POST("ParkingArea/parkingArea")
    Call<List<ParkingAreaDto>> findParkingAreas(@Header("Authorization") String sessionId, @Body Point geoPoint);

    @GET("sensorstate/{parkingSaceName}")
    Call<SensorStateDto> findParkingSpaceState(@Path("parkingSaceName") String parkingSaceName);

    @POST("sensorstate")
    Call<Void> changeState(@Header ("Content-Type") String ct, @Body String state);

    @GET("PayType/payTypes")
    Call<List<PayTypeDto>> getPayTypes();

    @GET("parking/parkings")
    Call<List<ParkingModel>> getParkingModel();

    @GET("VehicleType/mys")
    Call<List<VehicleDto>> getVechicles(@Header("Authorization") String sessionId);

    @GET("VehicleType/vehicleTypes2")
    Call<List<VehicleTypeDto>> getVechiclesTypes();

    @POST("VehicleType/plate")
    Call<VehicleDto> save(@Header("Authorization") String sessionId, @Body VehicleDto dto);

    @POST("VehicleType/plateDelete")
    Call<VehicleDto> deletePlate(@Header("Authorization") String sessionId, @Body VehicleDto dto);
}
