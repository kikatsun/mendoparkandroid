package com.mendopark.android.service.payment;

import com.mendopark.android.dto.payment.TarjetDto;
import com.mendopark.android.service.Preferencias;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class TarjetManager {

    private List<String> cardNumberFinal;
    private List<TarjetDto> tarjets;

    public static TarjetManager instance;

    private TarjetManager() {
        cardNumberFinal = new ArrayList<>();
        tarjets = new ArrayList<>();
        HashSet<String> cards = Preferencias.getInstanciaSinContexto().getCreditTarjets(
            Preferencias.getInstanciaSinContexto().getCreditsNumber()
        );
        if (cards.size() > 0) {
            for (String card: cards) {
                TarjetDto tarjet = new TarjetDto(card);
                if (tarjet != null && tarjet.getCardNumber() != null && !tarjet.getCardNumber().isEmpty()) {
                    String cardNumber = tarjet.getCardNumber();
                    cardNumber = cardNumber.substring(cardNumber.length() - 4);
                    cardNumber = "**** "+cardNumber;
                    if (!cardNumberFinal.contains(cardNumber)) {
                        cardNumberFinal.add(cardNumber);
                    }
                    tarjets.add(tarjet);
                }
            }
        }
    }

    public static TarjetManager getInstance() {
        if (instance == null) {
            instance = new TarjetManager();
        }
        return instance;
    }

    public void addCreditTarjet (TarjetDto tarjet) {
        if (tarjet != null && tarjet.getCardNumber() != null && !tarjet.getCardNumber().isEmpty()) {
            String cardNumber = tarjet.getCardNumber();
            cardNumber = cardNumber.substring(cardNumber.length() - 4);
            cardNumber = "**** "+cardNumber;
            if (!cardNumberFinal.contains(cardNumber)) {
                cardNumberFinal.add(cardNumber);
                tarjets.add(tarjet);
                Preferencias.getInstanciaSinContexto().addCreditTarjet(tarjet, tarjets.size());
            }
        }
    }

    public void updateCreditTarjet (TarjetDto tarjet) {
        if (tarjet != null && tarjet.getCardNumber() != null && !tarjet.getCardNumber().isEmpty()) {

        }
    }

    public List<TarjetDto> getTarjets() {
        return tarjets;
    }

    public List<String> getCardNumberFinal() {
        return cardNumberFinal;
    }
}
