package com.mendopark.android.service.rest.mendopark;

import com.mendopark.android.dto.UserDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserApi {

    @POST("User/persist")
    Call<UserDto> signIn(@Body UserDto newUser);

}
