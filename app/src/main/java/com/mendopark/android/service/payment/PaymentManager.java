package com.mendopark.android.service.payment;

import com.mendopark.android.dto.payment.PagoDto;

import java.util.List;

public class PaymentManager {

    public static PaymentManager instance;

    private List<PagoDto> historialPagos;
    private PagoDto nuevoPago;

    private PaymentManager(){
    }

    public static PaymentManager getInstance(){
        if (instance == null) {
            instance = new PaymentManager();
        }
        return instance;
    }

    public List<PagoDto> getHistorialPagos() {
        return historialPagos;
    }

    public void setHistorialPagos(List<PagoDto> historialPagos) {
        this.historialPagos = historialPagos;
    }

    public PagoDto getNuevoPago() {
        return nuevoPago;
    }

    public void setNuevoPago(PagoDto nuevoPago) {
        this.nuevoPago = nuevoPago;
    }
}
