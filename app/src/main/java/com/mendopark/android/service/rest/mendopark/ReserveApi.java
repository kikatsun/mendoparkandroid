package com.mendopark.android.service.rest.mendopark;

import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.dto.reserve.ReservaDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ReserveApi {

    @POST("parking/reserva")
    Call<ReservaDto> createReserve (@Body ReservaDto reserva);

    @POST("parking/reserva/cancel")
    Call<ReservaDto> cancel (@Body ReservaDto reserva);

    @POST("sensorstate/fromUser")
    Call<String> changeState(@Body String state);
}
