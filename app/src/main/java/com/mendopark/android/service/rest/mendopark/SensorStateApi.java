package com.mendopark.android.service.rest.mendopark;

import com.mendopark.android.dto.MendoParkSessionDto;
import com.mendopark.android.dto.payment.PayTypeDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SensorStateApi {

    @POST("sensorstate/fromUser")
    Call<String> changeState(@Body String state);

}
