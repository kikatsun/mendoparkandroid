package com.mendopark.android.service.reservation;

import com.mendopark.android.dto.reserve.ReservaDto;
import com.mendopark.android.model.ReserveState;

import java.util.HashMap;
import java.util.Map;

public class ReservationManager {

    public static ReservationManager instance = new ReservationManager();
    private ReservaDto reservaNueva;
    private Map<String, ReservaDto> reservasHistoricas = new HashMap<>();


    private ReservationManager(){}

    public static ReservationManager getInstance () {
        if (instance == null) {
            instance = new ReservationManager();
        }
        return instance;
    }

    public void crearReserva(String areaName, String parkingName, String parkingSapceId, Long parkingAreaNUmber, Float price){
        reservaNueva = new ReservaDto();
        reservaNueva.setParkingSpaceName(parkingName);
        reservaNueva.setParkingAreaName(areaName);
        reservaNueva.setCreationDate(System.currentTimeMillis());
        reservaNueva.setStateId(ReserveState.CREATED.getStateId());
        reservaNueva.setParkingAreaNumber(parkingAreaNUmber);
        reservaNueva.setIdPlaza(parkingSapceId);
        if (price == null) {
            price = 25F;
        }
        reservaNueva.setPriceByHour(price.floatValue());
    }

    public ReservaDto getReservaNueva() {
        return reservaNueva;
    }

    public void updateReserve(ReservaDto reserveUpdated) {
        this.reservaNueva = reserveUpdated;
    }

    public void clearReservas(){
        this.reservasHistoricas = new HashMap<>();
    }

    public void addHistoric (ReservaDto reserva) {
        reservasHistoricas.put(reserva.getId(), reserva);
    }

}
