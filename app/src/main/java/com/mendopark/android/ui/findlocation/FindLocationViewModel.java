package com.mendopark.android.ui.findlocation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.Marker;
import com.mendopark.android.dto.find.GeoPoint;
import com.mendopark.android.dto.find.GeoPointConsulted;
import com.mendopark.android.dto.find.ParkingAreaDto;
import com.mendopark.android.dto.find.ParkingSpaceDto;
import com.mendopark.android.dto.find.SensorDto;
import com.mendopark.android.dto.find.SensorStateDto;
import com.mendopark.android.model.SensorState;
import com.mendopark.android.repository.FindLocationRepository;
import com.mendopark.android.service.androidx.lifecycle.MapLiveData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FindLocationViewModel extends ViewModel {

    private FindLocationRepository findLocationRepository;
    private MutableLiveData<GeoPoint> centerPoint = new MutableLiveData<>();
    private MutableLiveData<List<ParkingAreaDto>> parkingAreas= new MutableLiveData<>();
    private MutableLiveData<List<SensorStateDto>> sensors = new MutableLiveData<>();
    private MutableLiveData<List<String>> parkingSpaces = new MutableLiveData<>(); // Ids de parkingSpaces
    private Map<String, MutableLiveData<SensorStateDto>> parkingSpacesStates = new HashMap<>();
    private MutableLiveData<Float> clientPrice = new MutableLiveData<>();
    private MutableLiveData<String> horario = new MutableLiveData<>();
    private MutableLiveData<String> targetState = new MutableLiveData<>();
    private MutableLiveData<String> parkingAreaNameTarget = new MutableLiveData<>();
    private MutableLiveData<String> parkingSpaceNameTarget = new MutableLiveData<>();
    private Map<String, String> parkingSpaceIdName = new HashMap<>();
    
    private MutableLiveData<Boolean> availableFree = new MutableLiveData<>();
    private MutableLiveData<Boolean> availableBussy = new MutableLiveData<>();

    private MutableLiveData<Boolean> availableReserve = new MutableLiveData<>();

    private Map<String, Float> clientsPrice = new HashMap<>();
    private Map<String, String> clientsHorario = new HashMap<>();

    private Map<String, Marker> markers = new HashMap<>();

    public FindLocationViewModel(FindLocationRepository findLocationRepository) {
        this.findLocationRepository = findLocationRepository;
//        centerPoint = new MutableLiveData<>();
//        centerPoint.setValue(new GeoPoint(-32.889735, -68.844612));

    }

    public LiveData<GeoPoint> getCenterPoint() {
        return this.centerPoint;
    }

    public LiveData<List<ParkingAreaDto>> getParkingAreas(){
        return this.parkingAreas;
    }

    public LiveData<List<String>> getParkingSpaces(){
        return this.parkingSpaces;
    }

    public void setCenterPoint(GeoPoint centerPoint){
        this.centerPoint.setValue(centerPoint);
    }

    public void setParkingAreas(List<ParkingAreaDto> parkingAreas){
        this.parkingAreas.setValue(parkingAreas);
    }

    /* Los dos mapas como clave usan el id de la plaza */
    public void setParkingSpaces(Map<String, String> parkingSpacesNames, Map<String, Marker> parkingSpacesMarker){
        this.markers = parkingSpacesMarker;
        List<String> parkingSpacesIds = new ArrayList<>(parkingSpacesNames.keySet());
        if(parkingSpacesIds.size() > 0){
            for (String parkingSpaceId: parkingSpacesIds ) {
                SensorStateDto sensorStateDto = new SensorStateDto();
                sensorStateDto.setName(parkingSpaceId);
                sensorStateDto.setState(SensorState.State.STATELESS);
                putParkingSpaceState(parkingSpaceId, sensorStateDto);
            }
        }
        this.parkingSpaceIdName = parkingSpacesNames;
        this.parkingSpaces.setValue(new ArrayList<>(parkingSpacesNames.keySet()));
    }

    public MutableLiveData<GeoPointConsulted> findLocation(String location){
        return findLocationRepository.findLocation(location);
    }

    public MutableLiveData<List<ParkingAreaDto>> findParkingAreas(GeoPoint point){
        return findLocationRepository.findParkingAreas(point);
    }

    public List<LiveData<SensorStateDto>> findParkingSpacesStates(){
        List<LiveData<SensorStateDto>> result = new ArrayList<>();
        for (String parkingSpaceId: parkingSpacesStates.keySet() ) {
            result.add(findLocationRepository.findParkingSpaceState(parkingSpaceId));
        }
        return result;
    }

    public void putParkingSpaceState (String parkingSpaceId, SensorStateDto sensorStateDto){
        MutableLiveData<SensorStateDto> sensorDtoLiveData = new MutableLiveData<>();
        sensorDtoLiveData.setValue(sensorStateDto);
        this.parkingSpacesStates.put(parkingSpaceId,sensorDtoLiveData);
    }

    public void updateParkingSpaceState (String parkingSpaceName, SensorStateDto sensorStateDto){
        MutableLiveData<SensorStateDto> sensorDtoLiveData = this.parkingSpacesStates.get(parkingSpaceName);
        sensorDtoLiveData.setValue(sensorStateDto);
    }

    public LiveData<SensorStateDto> getParkingSpaceState (String parkingSpaceId){
        return this.parkingSpacesStates.get(parkingSpaceId);
    }

    public void updateMarker (String parkingSpaceName, Marker marker){
        this.markers.put(parkingSpaceName, marker);
    }

    public Marker getMarker (String parkingSpaceName){
        return markers.get(parkingSpaceName);
    }

    public String getParkingSpaceName(String parkingSpaceId){
        return parkingSpaceIdName.get(parkingSpaceId);
    }

    public String getParkingSpaceId(String parkingSpaceName){
        Iterator<String> iterator = parkingSpaceIdName.keySet().iterator();
        String targetId = null;
        while (iterator.hasNext()) {
            String id = iterator.next();
            if (parkingSpaceName.equals(parkingSpaceIdName.get(id))) {
                targetId = id;
                break;
            }
        }
        return targetId;
    }

    public void setPrice(Float price) {
        clientPrice.setValue(price);
    }

    public void setHorario (String horario) {
        this.horario.setValue(horario);
    }

    public void setTargetState (String state) {
        targetState.setValue(state);
    }

    public MutableLiveData<Float> getClientPrice() {
        return clientPrice;
    }

    public MutableLiveData<String> getHorario() {
        return horario;
    }

    public MutableLiveData<String> getTargetState() {
        return targetState;
    }

    public MutableLiveData<String> getParkingAreaNameTarget() {
        return parkingAreaNameTarget;
    }

    public void setParkingAreaNameTarget(String parkingAreaNameTarget) {
        this.parkingAreaNameTarget.setValue(parkingAreaNameTarget);
    }

    public MutableLiveData<String> getParkingSpaceNameTarget() {
        return parkingSpaceNameTarget;
    }

    public void setParkingSpaceNameTarget(String parkingSpaceNameTarget) {
        this.parkingSpaceNameTarget.setValue(parkingSpaceNameTarget);
    }

    public void addCLientPrice (String name, Float price) {
        if (price != null) {
            clientsPrice.put(name, price);
        }
    }

    public void addCLientHorario (String name, String horario) {
        if (horario != null) {
            clientsHorario.put(name, horario);
        }
    }

    public Float getPrice (String name) {
        return clientsPrice.get(name);
    }

    public String getHOrario (String name) {
        return clientsHorario.get(name);
    }

    public MutableLiveData<Boolean> getAvailableReserve() {
        return availableReserve;
    }

    public void setAvailableReserve(Boolean availableReserve) {
        this.availableReserve.setValue(availableReserve);
    }

    public void setAvailableFree(Boolean availableFree) {
        this.availableFree.setValue(availableFree);
    }

    public void setAvailableBussy(Boolean availableBussy) {
        this.availableBussy.setValue(availableBussy);
    }

    public MutableLiveData<Boolean> getAvailableFree() {
        return availableFree;
    }

    public MutableLiveData<Boolean> getAvailableBussy() {
        return availableBussy;
    }

    public String changeState(String newState) {
        String result = parkingSpaceNameTarget.getValue();
        result = getParkingSpaceId(result);
        return result+newState;
    }

    public Long findParkingAreaNumber(String parkingAreaName){
        Long result = null;
        if(parkingAreas != null & parkingAreas.getValue() != null && parkingAreas.getValue().size() > 0){
            for (ParkingAreaDto p: parkingAreas.getValue()  ) {
                if(parkingAreaName.equals(p.getName())){
                    result = p.getNumber();
                }
            }
        }
        return result;
    }
}