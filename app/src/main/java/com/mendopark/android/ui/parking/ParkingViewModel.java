package com.mendopark.android.ui.parking;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.dto.ParkingModel;
import com.mendopark.android.repository.FindLocationRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ParkingViewModel extends ViewModel {
    private FindLocationRepository repository;

    private MutableLiveData<List<ParkingModel>> parkingLiveData = new MutableLiveData<>();

    public ParkingViewModel(FindLocationRepository repository) {
        this.repository = repository;
    }


    public MutableLiveData<List<ParkingModel>> findParking() {
        return repository.findParking();
    }


    public List<ParkingModel> getFakeData(){
        List <ParkingModel> parkingModels = new ArrayList<>();
        ParkingModel parkingModel = new ParkingModel();

        parkingModel.setId(new UUID(15651321,5165132).randomUUID());
        parkingModel.setParkingSpaceName("cny70Calle");
        parkingModel.setStart(new Date());
        parkingModel.setEnd(new Date());
        parkingModel.setHours(8);




        ParkingModel parkingModel2 = new ParkingModel();

        parkingModel2.setId(new UUID(15651321,5165132).randomUUID());
        parkingModel2.setParkingSpaceName("Fake 2");
        parkingModel2.setStart(new Date());
        parkingModel2.setEnd(new Date());
        parkingModel2.setHours(8);

        parkingModels.add(parkingModel);
        parkingModels.add(parkingModel2);
        return parkingModels;
    }
}
