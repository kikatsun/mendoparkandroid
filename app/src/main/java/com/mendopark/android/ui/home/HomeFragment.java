package com.mendopark.android.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mendopark.android.R;
import com.mendopark.android.adapters.StepperAdapter;
import com.stepstone.stepper.StepperLayout;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private StepperLayout mStepperLayout;
    private StepperAdapter mStepperAdapter;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        mStepperLayout = root.findViewById(R.id.stepperLayout);
        mStepperAdapter = new StepperAdapter(getFragmentManager(), getContext());
        mStepperLayout.setCompleteButtonEnabled(false);
        mStepperLayout.setAdapter(mStepperAdapter);
        return root;
    }
}