package com.mendopark.android.ui.plate;

import android.widget.Button;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.dto.plate.VehicleDto;
import com.mendopark.android.dto.plate.VehicleTypeDto;
import com.mendopark.android.repository.FindLocationRepository;

import java.util.ArrayList;
import java.util.List;

public class PlateViewModel extends ViewModel {

    private FindLocationRepository repository;

    private MutableLiveData<List<VehicleDto>> vehicles = new MutableLiveData<>();
    private MutableLiveData<List<VehicleTypeDto>> vehiclesTypes = new MutableLiveData<>();
    private MutableLiveData<VehicleDto> vechicle = new MutableLiveData<>();
    private MutableLiveData<String> selectedVechicle = new MutableLiveData<>();
    private MutableLiveData<Integer> typeSelected = new MutableLiveData<>();

    private MutableLiveData<Button> guardar = new MutableLiveData<>();

    public PlateViewModel(FindLocationRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<List<VehicleDto>> findPayTypes() {
        vehicles = repository.findVehicles();
        return vehicles;
    }

    public void setVehicles(List<VehicleDto> news){
        vehicles.setValue(news);
    }

    public MutableLiveData<List<VehicleTypeDto>> findVehiclesTypes() {
        vehiclesTypes = repository.findVehiclesTypes();
        return vehiclesTypes;
    }

    public void setVechicle(VehicleDto dto){
        vechicle.setValue(dto);
    }

    public MutableLiveData<VehicleDto> getVechicle(){
        return vechicle;
    }

    public void selectVechile(String domain){
        selectedVechicle.setValue(domain);
    }

    public MutableLiveData<String> getSelectedVechicle(){
        return selectedVechicle;
    }

    public void updateSelectVehicle(String domain) {
        List<VehicleDto> vv = vehicles.getValue();
        if(vv != null && vv.size() > 0){
            for (int i = 0; i < vv.size(); i++) {
                VehicleDto v = vv.get(i);
                if(v.getDomain() != null && v.getDomain().equals(domain)){
                    setVechicle(v);
                    if(v.getType() != null){
                        typeSelected.setValue(v.getType());
                    }
                }
            }
        }
    }

    public void updateSelectVehicleType(Integer numberType, String domain, String brand, String color, String model, String anio) {
        VehicleDto v = vechicle.getValue();
        v.setType(numberType);
        v.setDomain(domain);
        v.setBrand(brand);
        v.setModel(model);
        v.setColor(color);
        try {
            v.setYear(Integer.valueOf(anio));
        }catch (NumberFormatException ex){}
        vechicle.setValue(v);
    }

    public MutableLiveData<Integer> getTypeSelected(){
        return typeSelected;
    }

    public MutableLiveData<Button> getGuardar(){
        return guardar;
    }

    public MutableLiveData<VehicleDto> saveNewPlate(String domain, String brand, String color, String model, String anio) {
        VehicleDto vv = vechicle.getValue();
        vv.setDomain(domain);
        vv.setBrand(brand);
        vv.setModel(model);
        vv.setColor(color);
        try {
            vv.setYear(Integer.valueOf(anio));
        }catch (NumberFormatException ex){}
        return repository.savePLate(vv);

//        List<VehicleDto> vv = vehicles.getValue();
//        VehicleDto nuevo = vechicle.getValue();
//        if(nuevo != null) {
//            if (vv == null) {
//                vv = new ArrayList<>();
//            }
//            VehicleDto target = null;
//            for (int i = 0; i < vv.size(); i++) {
//                VehicleDto v = vv.get(i);
//                if (v.getDomain().equals(nuevo.getDomain())) {
//                    target = v;
//                    break;
//                }
//            }
//            if (target == null) {
//                vv.add(nuevo);
//                vehicles.setValue(vv);
//            } else {
//                List<VehicleDto> newList = new ArrayList<>();
//                VehicleDto v = vechicle.getValue();
//                if (v != null && v.getDomain() != null) {
//                    for (int i = 0; i < vv.size(); i++) {
//                        if (!v.getDomain().equals(vv.get(i).getDomain())) {
//                            newList.add(vv.get(i));
//                        }
//                    }
//                }
//                newList.add(nuevo);
//                vehicles.setValue(newList);
//            }
//            vechicle.setValue(null);
//        }
    }

    public MutableLiveData<VehicleDto> eliminarPLate(String domain) {
        VehicleDto vv = new VehicleDto();
        vv.setDomain(domain);
        return repository.eliminarPLate(vv);
//        List<VehicleDto> vv = vehicles.getValue();
//        List<VehicleDto> newList = new ArrayList<>();
//        if(vv != null && vv.size() > 0) {
//            VehicleDto v  = vechicle.getValue();
//            if(v != null && v.getDomain() != null){
//                for (int i = 0; i < vv.size(); i++) {
//                    if(!v.getDomain().equals(vv.get(i).getDomain())){
//                        newList.add(vv.get(i));
//                    }
//                }
//            }
//            vehicles.setValue(newList);
//        }
//        vechicle.setValue(null);
    }
}
