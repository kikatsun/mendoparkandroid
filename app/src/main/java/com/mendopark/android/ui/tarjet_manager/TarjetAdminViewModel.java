package com.mendopark.android.ui.tarjet_manager;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TarjetAdminViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TarjetAdminViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is send fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}