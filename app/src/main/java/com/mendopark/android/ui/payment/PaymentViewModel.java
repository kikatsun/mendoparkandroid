package com.mendopark.android.ui.payment;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.dto.payment.PagoDto;
import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.repository.FindLocationRepository;

import java.util.List;

public class PaymentViewModel extends ViewModel {

    private FindLocationRepository repository;

    private MutableLiveData<List<PayTypeDto>> payTypes = new MutableLiveData<>();
    private MutableLiveData<PagoDto> pago = new MutableLiveData<>();
    private MutableLiveData<Long> payTypeSelected = new MutableLiveData<>();
    private MutableLiveData<String> tarjetSelected = new MutableLiveData<>();


    public PaymentViewModel(FindLocationRepository repository) {
        this.repository = repository;
    }

    public MutableLiveData<List<PayTypeDto>> findPayTypes() {
        return repository.findPayTypes();
    }

    public void setPayTypes(List<PayTypeDto> payTypes) {
        this.payTypes.setValue(payTypes);
    }

    public MutableLiveData<PagoDto> getPago() {
        return pago;
    }

    public void setPago(PagoDto pago) {
        this.pago.setValue(pago);
    }

    public MutableLiveData<Long> getPayTypeSelected() {
        return payTypeSelected;
    }

    public void setPayTypeSelected(Long payTypeSelected) {
        this.payTypeSelected.setValue(payTypeSelected);
    }

    public MutableLiveData<String> getTarjetSelected() {
        return tarjetSelected;
    }

    public void setTarjetSelected(String tarjetSelected) {
        this.tarjetSelected.setValue(tarjetSelected);
    }
}
