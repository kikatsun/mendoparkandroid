package com.mendopark.android.ui.sign_in;

import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.R;
import com.mendopark.android.dto.UserDto;
import com.mendopark.android.model.Role;
import com.mendopark.android.repository.SignInRepository;

import java.util.HashMap;
import java.util.Map;

public class SignInViewModel extends ViewModel {

    private static final Integer USER_ROLE_FROM_ANDROID = Role.COMMON_USER.getRoleId();

    private MutableLiveData<SignInFormState> signInFormState = new MutableLiveData<>();
    private MutableLiveData<UserDto> signInUser = new MutableLiveData<>();
    private SignInRepository signInRepository;
    private LiveData<UserDto> newUserResult;
    private UserDto newUser;

    SignInViewModel(SignInRepository signInRepository){
        this.signInRepository = signInRepository;
        newUser = new UserDto();
    }

    public LiveData<SignInFormState> getSignInFormState(){
        return this.signInFormState;
    }

    public void signInDataChange(String name, String lastName, String userName, String password,
         String rePassword, String email, String documentNumber){

        Map<SignInFormState.Field, Integer> invalidFields = new HashMap<>();

        if(!isNameValid(name)){
            invalidFields.put(SignInFormState.Field.NAME, R.string.sign_in_invalid_name);
        }else if(!isLastNameValid(lastName)){
            invalidFields.put(SignInFormState.Field.LAST_NAME, R.string.sign_in_invalid_last_name);
        }else if(!isUserNameValid(userName)){
            invalidFields.put(SignInFormState.Field.USER_NAME, R.string.sign_in_invalid_user_name);
        }else if(!isPasswordValid(password)){
            invalidFields.put(SignInFormState.Field.PASSWORD, R.string.sign_in_invalid_password);
        }else if(!isRePasswordValid(rePassword,password)){
            invalidFields.put(SignInFormState.Field.RE_PASSWORD, R.string.sign_in_invalid_re_password);
        }else if(!isEmailValid(email)){
            invalidFields.put(SignInFormState.Field.EMAIL, R.string.sign_in_invalid_email);
        }else if(!isDocumentNumberValid(documentNumber)){
            invalidFields.put(SignInFormState.Field.DOCUMENT_NUMBER, R.string.sign_in_invalid_document_number);
        }

        if(invalidFields.size() == 0){
            signInFormState.setValue(new SignInFormState(true));
            newUser.setName(name);
            newUser.setLastName(lastName);
            newUser.setUserName(userName);
            newUser.setPassword(password);
            newUser.setEmail(email);
            newUser.setDocumentNumber(documentNumber);
            newUser.setRoleId(USER_ROLE_FROM_ANDROID);
        }else{
            signInFormState.setValue(new SignInFormState(invalidFields));
        }
    }

    public void signIn(){
        newUserResult = signInRepository.signIn(newUser);
    }

    public LiveData<UserDto> getNewUserResult(){
        return this.newUserResult;
    }

    private boolean isNameValid(String name){
        return name != null && name.trim().length() > 2;
    }

    private boolean isLastNameValid(String lastName){
        return lastName != null && lastName.trim().length() > 2;
    }

    private boolean isUserNameValid(String userName){
        return userName != null && userName.trim().length() > 4;
    }

    private boolean isPasswordValid(String password){
        return password != null && password.trim().length() > 5;
    }

    private boolean isRePasswordValid(String rePassword, String password){
        return rePassword != null && password != null && rePassword.equals(password);
    }

    private boolean isEmailValid(String email){
        return email != null && email.contains("@") && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isDocumentNumberValid(String documentNumber){
        return documentNumber != null && documentNumber.trim().length() == 8;
    }

}
