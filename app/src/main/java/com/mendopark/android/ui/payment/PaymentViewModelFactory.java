package com.mendopark.android.ui.payment;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.repository.FindLocationRepository;

public class PaymentViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(PaymentViewModel.class)){
            return (T) new PaymentViewModel(FindLocationRepository.getInstance(new FindLocationDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class: "+modelClass);
        }
    }
}
