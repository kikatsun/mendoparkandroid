package com.mendopark.android.ui.log_in;

import android.os.Handler;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.dto.MendoParkSessionDto;
import com.mendopark.android.repository.LogInRepository;

import java.util.UUID;

public class LogInViewModel extends ViewModel {

    private LogInRepository logInRepository;
    final Handler handler= new Handler();

    private MutableLiveData<MendoParkSessionDto> session = new MutableLiveData<>();

    LogInViewModel (LogInRepository logInRepository) {
        this.logInRepository = logInRepository;
    }

    public LiveData<MendoParkSessionDto> getSession() {
        return session;
    }

    public LiveData<MendoParkSessionDto> logInAction (String userName, String password) {
        MendoParkSessionDto sessionDto = new MendoParkSessionDto();
        sessionDto.setUserName(userName);
        sessionDto.setPassword(password);
        return logInRepository.createSession(sessionDto);//empezara a ejecutarse después de 5 milisegundos
    }

    private MendoParkSessionDto setRandomSession(){
        MendoParkSessionDto sessionDto = new MendoParkSessionDto();
        sessionDto.setSessionId(UUID.randomUUID().toString());
        return sessionDto;
    }

    public void stopHandler(){
        handler.removeCallbacks(myRunner);
    }

    Runnable myRunner = new Runnable() {
        @Override
        public void run() {
            session.postValue(setRandomSession());//llamamos nuestro metodo
            handler.postDelayed(this,5000);//se ejecutara cada 5 segundos
        }

    };
}
