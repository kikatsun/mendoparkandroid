package com.mendopark.android.ui.reserve;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.mendopark.android.R;
import com.mendopark.android.dto.payment.PagoDto;
import com.mendopark.android.dto.reserve.ReservaDto;
import com.mendopark.android.model.ReserveState;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.payment.PaymentManager;
import com.mendopark.android.service.reservation.ReservationManager;
import com.mendopark.android.ui.findlocation.FindLocationViewModelFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReserveFragment extends Fragment {

    private ReserveViewModel reserveViewModel;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/aaaa");
    private final Long MINT_TO_RESERVE = 1800000L;
    private final Long MINT_TO_RESERVE_TEST = 1800000L;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        reserveViewModel =
                ViewModelProviders.of(this,new ReserveViewModelFactory()).get(ReserveViewModel.class);
        View root = inflater.inflate(R.layout.fragment_reserve, container, false);

        ReservaDto reserva = ReservationManager.getInstance().getReservaNueva();
        if (reserva != null) {
            reserveViewModel.updateReserve(reserva);
            final TextView parkingAreaName = root.findViewById(R.id.nombreEstacionamiento);
            final TextView parkingSpaceName = root.findViewById(R.id.nombrePlaza);
            final TextView price = root.findViewById(R.id.precioPorHora);
            final TextView anticipateAmount = root.findViewById(R.id.montoAnticipo);
            final TextView patent = root.findViewById(R.id.textView14);
            final TextView observations = root.findViewById(R.id.textView15);

            final TextView messageReserve = root.findViewById(R.id.reservado_mesage);

            final EditText patente = root.findViewById(R.id.patente);
            final EditText obs = root.findViewById(R.id.observaciones);

            final Button reserveButton = root.findViewById(R.id.reservarBtn);
            final Button reservePaymentButton = root.findViewById(R.id.pagar_reserva_btn);
            final Button cancelReservetButton = root.findViewById(R.id.cancelarreservarBtn);
            parkingAreaName.setText(reserva.getParkingAreaName());
            parkingSpaceName.setText(reserva.getParkingSpaceName());
            price.setText(String.valueOf(reserva.getPriceByHour()));
            anticipateAmount.setText(String.valueOf(reserva.getAnticipatedAmount()));
            if (ReserveState.CREATED.getStateId() == reserva.getStateId()) {
                patente.setEnabled(true);
                obs.setEnabled(true);
                reserveButton.setEnabled(true);


                if (reserva.getId() == null) {
                    if (reserva.getPago() != null) {
                        PagoDto pagoDto = PaymentManager.getInstance().getNuevoPago();
                        if (pagoDto != null && pagoDto.getConcepto() != null &&
                                pagoDto.getConcepto().equals(reserva.getParkingSpaceName())) {
                            reserva.setPago(pagoDto);
                        }
                    }
                    if (reserva.getPago() != null && reserva.getPago().getId() != null
                            && !reserva.getPago().getId().isEmpty()) {

                        reserveButton.setVisibility(View.VISIBLE);
                        reservePaymentButton.setText("PAGADO");
                        reservePaymentButton.setVisibility(View.VISIBLE);
                        reservePaymentButton.setEnabled(false);
                        patente.setVisibility(View.VISIBLE);
                        patent.setVisibility(View.VISIBLE);
                        obs.setVisibility(View.VISIBLE);
                        observations.setVisibility(View.VISIBLE);
                        reserveButton.setEnabled(false);
                        reserveButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reserva.setPlate(patente.getText().toString());
                                reserva.setObservations(obs.getText().toString());
                                reserveViewModel.reserveAction(reserva).observe(getActivity(), new Observer<ReservaDto>() {
                                    @Override
                                    public void onChanged(ReservaDto reservaDto) {
                                        ReservationManager.getInstance().updateReserve(reservaDto);
                                        if (reservaDto.getId() != null) {
                                            reserveButton.setText("RESERVADO");
                                            reserveButton.setEnabled(false);
                                            patente.setEnabled(false);
                                            obs.setEnabled(false);
                                            if (reservaDto.getCreationDate() == null) {
                                                reservaDto.setCreationDate(System.currentTimeMillis());
                                            }
                                            reservaDto.setStateId(ReserveState.ACEPTED.getStateId());
                                            ReservationManager.getInstance().updateReserve(reservaDto);
                                            messageReserve.setText("Reserva creada correctamente: \n" + reservaDto.getId() + "\n" +
                                                    "Por favor llegar al estacionamiento antes de\n " + simpleDateFormat.format(new Date(reservaDto.getCreationDate().longValue() + MINT_TO_RESERVE_TEST)));
                                            messageReserve.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                            }
                        });

                        TextWatcher afterChengePatent = new TextWatcher() {

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                boolean validate = reserveViewModel.validatePatent(patente.getText().toString());
                                if (!validate) {
                                    patente.setError("Formato permitido: XXX### ó XX####XX. Ejemplo TGF456 ó we4566po");
                                } else {
                                    patente.setError(null);
                                }
                            }
                        };

                        patente.addTextChangedListener(afterChengePatent);

                        reserveViewModel.getPatentValidation().observe(getActivity(), new Observer<Boolean>() {
                            @Override
                            public void onChanged(Boolean aBoolean) {
                                if (!aBoolean) {
                                    patente.setError("Formato permitido: XXX### ó XX####XX. Ejemplo TGF456 ó we4566po");
                                } else {
                                    patente.setError(null);
                                    reserveButton.setEnabled(aBoolean);
                                }
                            }
                        });
                        cancelReservetButton.setVisibility(View.VISIBLE);
                        cancelReservetButton.setEnabled(true);
                        cancelReservetButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reserveViewModel.cancelReserve(reserva).observe(getActivity(), new Observer<ReservaDto>() {
                                    @Override
                                    public void onChanged(ReservaDto reservaDto) {
                                        cancelReservetButton.setVisibility(View.INVISIBLE);
                                        reservaDto.setStateId(ReserveState.CANCELLED.getStateId());
                                        ReservationManager.getInstance().updateReserve(reservaDto);
                                        reserveButton.setText("Cancelado");
                                        reserveButton.setEnabled(false);
                                        messageReserve.setVisibility(View.INVISIBLE);
                                    }
                                });
                            }
                        });
                    } else {
                        reservePaymentButton.setVisibility(View.VISIBLE);
                        reservePaymentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PagoDto newPayment = new PagoDto();
                                newPayment.setMonto(reserva.getAnticipatedAmount());
                                newPayment.setConcepto(reserva.getParkingSpaceName());
                                reserva.setPago(newPayment);
                                ReservationManager.getInstance().updateReserve(reserva);
                                PaymentManager.getInstance().setNuevoPago(newPayment);
                                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.nav_payment);
                            }
                        });
                    }
                } else {
                    final TextView titel = root.findViewById(R.id.reserve_title);
                    titel.setText("Reserva Actual");
                    if (reserva.getCreationDate() != null) {
                        messageReserve.setText("Reserva creada correctamente: " + reserva.getId() + "\n" +
                                "Por favor llegar al estacionamiento antes de " + simpleDateFormat.format(new Date(reserva.getCreationDate().longValue() + MINT_TO_RESERVE_TEST)));
                        messageReserve.setVisibility(View.VISIBLE);
                    }
                    patente.setText(reserva.getPlate());
                    patente.setEnabled(false);
                    obs.setText(reserva.getObservations());
                    obs.setEnabled(false);
                    cancelReservetButton.setVisibility(View.VISIBLE);
                    cancelReservetButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reserveViewModel.cancelReserve(reserva).observe(getActivity(), new Observer<ReservaDto>() {
                                @Override
                                public void onChanged(ReservaDto reservaDto) {
                                    cancelReservetButton.setVisibility(View.INVISIBLE);
                                    reservaDto.setStateId(ReserveState.CANCELLED.getStateId());
                                    ReservationManager.getInstance().updateReserve(reservaDto);
                                    reserveButton.setText("Cancelado");
                                    reserveButton.setEnabled(false);
                                    messageReserve.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    });
                }
            } else if (ReserveState.ACEPTED.getStateId() == reserva.getStateId()) {
                patente.setText(reserva.getPlate());
                obs.setText(reserva.getObservations());
                patente.setEnabled(false);
                patent.setVisibility(View.VISIBLE);
                patente.setVisibility(View.VISIBLE);
                obs.setEnabled(false);
                obs.setVisibility(View.VISIBLE);
                observations.setVisibility(View.VISIBLE);

                if (System.currentTimeMillis() - reserva.getCreationDate() > MINT_TO_RESERVE_TEST) {
                    reserveButton.setText("Cancelado");
                    reserveButton.setEnabled(false);
                    reserveButton.setVisibility(View.VISIBLE);
                    reserva.setStateId(ReserveState.CANCELLED.getStateId());
                    reserva.setStateObservation("El tiempo de la reserva se venció hace "+
                        String.valueOf(System.currentTimeMillis() - reserva.getCreationDate() / 60000)
                        + "minutos");
                    ReservationManager.getInstance().updateReserve(reserva);
                    cancelReservetButton.setVisibility(View.INVISIBLE);
                }else {
                    reserveButton.setText("RESERVADO");
                    reserveButton.setEnabled(false);
                    reserveButton.setVisibility(View.VISIBLE);
                    if (reserva.getCreationDate() == null) {
                        reserva.setCreationDate(System.currentTimeMillis());
                    }
                    reserva.setStateObservation("Reserva creada correctamente: \n" + reserva.getId() + "\n" +
                            "Por favor llegar al estacionamiento antes de\n " + simpleDateFormat.format(new Date(reserva.getCreationDate().longValue() + MINT_TO_RESERVE_TEST)));

                    cancelReservetButton.setVisibility(View.VISIBLE);
                    cancelReservetButton.setEnabled(true);
                    cancelReservetButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reserveViewModel.cancelReserve(reserva).observe(getActivity(), new Observer<ReservaDto>() {
                                @Override
                                public void onChanged(ReservaDto reservaDto) {
                                    cancelReservetButton.setVisibility(View.INVISIBLE);
                                    reservaDto.setStateId(ReserveState.CANCELLED.getStateId());
                                    ReservationManager.getInstance().updateReserve(reservaDto);
                                    reserveButton.setText("Cancelado");
                                    reserveButton.setEnabled(false);
                                    messageReserve.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    });
                }
                messageReserve.setText(reserva.getStateObservation());
                messageReserve.setVisibility(View.VISIBLE);
            } else if (ReserveState.REJECTED.getStateId() == reserva.getStateId()) {
                patente.setText(reserva.getPlate());
                patente.setEnabled(false);
                patent.setVisibility(View.VISIBLE);
                patente.setVisibility(View.VISIBLE);
                obs.setText(reserva.getObservations());
                obs.setEnabled(false);
                obs.setVisibility(View.VISIBLE);
                observations.setVisibility(View.VISIBLE);
                cancelReservetButton.setVisibility(View.INVISIBLE);
                reserveButton.setText("Rechazado");
                reserveButton.setEnabled(false);
                reserveButton.setVisibility(View.VISIBLE);
                messageReserve.setText("Reserva rechazada");
                messageReserve.setVisibility(View.VISIBLE);
            } else if (ReserveState.CANCELLED.getStateId() == reserva.getStateId()){
                patente.setText(reserva.getPlate());
                patente.setEnabled(false);
                patent.setVisibility(View.VISIBLE);
                patente.setVisibility(View.VISIBLE);
                obs.setText(reserva.getObservations());
                obs.setEnabled(false);
                obs.setVisibility(View.VISIBLE);
                observations.setVisibility(View.VISIBLE);
                cancelReservetButton.setVisibility(View.INVISIBLE);
                reserveButton.setText("Cancelado");
                reserveButton.setEnabled(false);
                reserveButton.setVisibility(View.VISIBLE);
                messageReserve.setText("Reserva cancelada");
                messageReserve.setVisibility(View.VISIBLE);
            }
        }
        return root;
    }

    private class CambiarEstado extends AsyncTask<String,Integer,Integer> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Integer doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ApiBuilder.MENDOPARK_URL + "sensorstate/fromUser");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
                writer.write(dto);
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try {
                    conection.connect();
                } catch (SocketTimeoutException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                } catch (ConnectException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while (read != null) {
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            } catch (MalformedURLException e) {
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return 0;

        }
    }
}