package com.mendopark.android.ui.tarjet_manager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.cooltechworks.creditcarddesign.CreditCardView;
import com.mendopark.android.R;
import com.mendopark.android.dto.payment.TarjetDto;
import com.mendopark.android.service.payment.TarjetManager;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class TarjetAdminFragment extends Fragment {

    private TarjetAdminViewModel tarjetAdminViewModel;
//

    private final int CREATE_NEW_CARD = 0;

    private LinearLayout cardContainer;
    private Button addCardButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        tarjetAdminViewModel =
                ViewModelProviders.of(this).get(TarjetAdminViewModel.class);
        View root = inflater.inflate(R.layout.fragment_payment, container, false);
        addCardButton = root.findViewById(R.id.add_card);
        cardContainer = root.findViewById(R.id.card_container);
        List<TarjetDto> tarjets = TarjetManager.getInstance().getTarjets();

        if (tarjets.size() > 0) {
            for (TarjetDto tarjet: tarjets) {

                CreditCardView sampleCreditCardView = new CreditCardView(getContext());

                sampleCreditCardView.setCVV(tarjet.getCvv());
                sampleCreditCardView.setCardHolderName(tarjet.getCardHolderName());
                sampleCreditCardView.setCardExpiry(tarjet.getExpiry());
                sampleCreditCardView.setCardNumber(tarjet.getCardNumber());

                cardContainer.addView(sampleCreditCardView);
                int index = cardContainer.getChildCount() - 1;
                addCardListener(index, sampleCreditCardView);
            }
        }

//        cardContainer = (LinearLayout) findViewById(R.id.card_container);
//        initialize();
        listeners();
        return root;
    }

//
//    private void initialize() {
//        addCardButton = (Button) findViewById(R.id.add_card);
//        cardContainer = (LinearLayout) findViewById(R.id.card_container);
////        getSupportActionBar().setTitle("Payment");
//        populate();
//    }
//
//    private void populate() {
//        CreditCardView sampleCreditCardView = new CreditCardView(getContext());
//
//        String name = "Glarence Zhao";
//        String cvv = "420";
//        String expiry = "01/18";
//        String cardNumber = "4242424242424242";
//
//        sampleCreditCardView.setCVV(cvv);
//        sampleCreditCardView.setCardHolderName(name);
//        sampleCreditCardView.setCardExpiry(expiry);
//        sampleCreditCardView.setCardNumber(cardNumber);
//
//        cardContainer.addView(sampleCreditCardView);
//        int index = cardContainer.getChildCount() - 1;
//        addCardListener(index, sampleCreditCardView);
//    }
//
    private void listeners() {
        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), CardEditActivity.class);
                startActivityForResult(intent, CREATE_NEW_CARD);
            }
        });
    }
//
    private void addCardListener(final int index, CreditCardView creditCardView) {
        creditCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CreditCardView creditCardView = (CreditCardView) v;
                String cardNumber = creditCardView.getCardNumber();
                String expiry = creditCardView.getExpiry();
                String cardHolderName = creditCardView.getCardHolderName();
                String cvv = creditCardView.getCVV();

                Intent intent = new Intent(getContext(), CardEditActivity.class);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME, cardHolderName);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_NUMBER, cardNumber);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_EXPIRY, expiry);
                intent.putExtra(CreditCardUtils.EXTRA_CARD_SHOW_CARD_SIDE, CreditCardUtils.CARD_SIDE_BACK);
                intent.putExtra(CreditCardUtils.EXTRA_VALIDATE_EXPIRY_DATE, false);

                // start at the CVV activity to edit it as it is not being passed
                intent.putExtra(CreditCardUtils.EXTRA_ENTRY_START_PAGE, CreditCardUtils.CARD_CVV_PAGE);
                startActivityForResult(intent, index);
            }
        });
    }
//
    public void onActivityResult(int reqCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
//            Debug.printToast("Result Code is OK", getApplicationContext());

            String name = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            String cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);


            TarjetDto tarjet = new TarjetDto();
            tarjet.setCardHolderName(name);
            tarjet.setCardNumber(cardNumber);
            tarjet.setCvv(cvv);
            tarjet.setExpiry(expiry);

            if (reqCode == CREATE_NEW_CARD) {

                CreditCardView creditCardView = new CreditCardView(getContext());

                creditCardView.setCVV(cvv);
                creditCardView.setCardHolderName(name);
                creditCardView.setCardExpiry(expiry);
                creditCardView.setCardNumber(cardNumber);

                cardContainer.addView(creditCardView);
                int index = cardContainer.getChildCount() - 1;
                addCardListener(index, creditCardView);
                TarjetManager.getInstance().addCreditTarjet(tarjet);
            } else {

                CreditCardView creditCardView = (CreditCardView) cardContainer.getChildAt(reqCode);

                creditCardView.setCardExpiry(expiry);
                creditCardView.setCardNumber(cardNumber);
                creditCardView.setCardHolderName(name);
                creditCardView.setCVV(cvv);
//                TarjetManager.getInstance().updateCreditTarjet(tarjet);

            }
        }

    }

}