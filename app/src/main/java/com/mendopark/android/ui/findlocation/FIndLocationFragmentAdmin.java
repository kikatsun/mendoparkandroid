package com.mendopark.android.ui.findlocation;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.mendopark.android.R;
import com.mendopark.android.dto.find.GeoPoint;
import com.mendopark.android.dto.find.GeoPointConsulted;
import com.mendopark.android.dto.find.ParkingAreaDto;
import com.mendopark.android.dto.find.ParkingSpaceDto;
import com.mendopark.android.dto.find.PolygonPointsDto;
import com.mendopark.android.dto.find.SensorDto;
import com.mendopark.android.dto.find.SensorStateDto;
import com.mendopark.android.model.SensorState;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.reservation.ReservationManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FIndLocationFragmentAdmin extends Fragment implements OnMapReadyCallback {

    private FindLocationViewModel findLocationViewModel;
    private GoogleMap googleMap;

    private Handler handler= new Handler();
    Runnable automatico;

    private Button iniciarBtn;
    private Button finalizarBtn;

    private class MarkerType{
        public static final String SENSOR = "SENSOR";
        public static final String PARKING_AREA = "PARKING_AREA";
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        findLocationViewModel =
                ViewModelProviders.of(this, new FindLocationViewModelFactory())
                        .get(FindLocationViewModel.class);
        View root = inflater.inflate(R.layout.fragment_find_admin, container, false);
        SupportMapFragment supportMapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        final EditText locationText = root.findViewById(R.id.findEditText);
        final Button findButton = root.findViewById(R.id.findButton);
//
        iniciarBtn = getActivity().findViewById(R.id.iniciar_estacionamiento_btn);
        iniciarBtn.setVisibility(View.INVISIBLE);
        finalizarBtn = getActivity().findViewById(R.id.finalizar_estacioneamiento_btn);
        finalizarBtn.setVisibility(View.INVISIBLE);
//        reserveButton.setOnClickListener(reserveAction);

        iniciarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stateMessage = findLocationViewModel.changeState("|OCUPADO");
                CambiarEstado cambiarEstado = new CambiarEstado();
                Integer res = null;
                try {
                    res = cambiarEstado.execute(stateMessage).get();
                    iniciarBtn.setVisibility(View.INVISIBLE);
                }
                catch(Exception e){
                    Toast.makeText(
                            getContext(),
                            "ERROR al Registrar Usuario",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });

        finalizarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stateMessage = findLocationViewModel.changeState("|LIBRE");
                CambiarEstado cambiarEstado = new CambiarEstado();
                Integer res = null;
                try {
                    res = cambiarEstado.execute(stateMessage).get();
                    finalizarBtn.setVisibility(View.INVISIBLE);
                }
                catch(Exception e){
                    Toast.makeText(
                            getContext(),
                            "ERROR al Registrar Usuario",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });

        final TextView parkingAreaName = getActivity().findViewById(R.id.menu2_parking_area_name);
        final TextView parkingSpaceName = getActivity().findViewById(R.id.menu2_parking_space_name);
        final TextView parkingSpacePrice = getActivity().findViewById(R.id.menu2_parking_space_price);
        final TextView parkingSpaceTime = getActivity().findViewById(R.id.menu2_parking_space_time);
        final TextView parkingSpaceState = getActivity().findViewById(R.id.menu2_parking_space_state);

        findLocationViewModel.getAvailableFree().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean != null) {
                    finalizarBtn.setVisibility(View.INVISIBLE);
                    iniciarBtn.setVisibility(View.VISIBLE);
                }else {
                    iniciarBtn.setVisibility(View.VISIBLE);
                    finalizarBtn.setVisibility(View.VISIBLE);
                }
            }
        });


        findLocationViewModel.getAvailableBussy().observe(getActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean != null) {
                    iniciarBtn.setVisibility(View.INVISIBLE);
                    finalizarBtn.setVisibility(View.VISIBLE);
                }else {
                    iniciarBtn.setVisibility(View.VISIBLE);
                    finalizarBtn.setVisibility(View.VISIBLE);
                }
            }
        });

        findLocationViewModel.getClientPrice().observe(getActivity(), new Observer<Float>() {
            @Override
            public void onChanged(Float aFloat) {
                if(aFloat != null) {
                    parkingSpacePrice.setText(String.valueOf(aFloat));
                }
            }
        });

        findLocationViewModel.getTargetState().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String aState) {
                if(aState != null) {
                    parkingSpaceState.setText(aState);
                }
            }
        });

        findLocationViewModel.getHorario().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String aHorario) {
                if(aHorario != null) {
                    parkingSpaceTime.setText(aHorario);
                }
            }
        });

        findLocationViewModel.getParkingAreaNameTarget().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String aAreaName) {
                if(aAreaName != null) {
                    parkingAreaName.setText(aAreaName);
                }
            }
        });

        findLocationViewModel.getParkingSpaceNameTarget().observe(getActivity(), new Observer<String>() {
            @Override
            public void onChanged(String aSpaceName) {
                if(aSpaceName != null) {
                    parkingSpaceName.setText(aSpaceName);
                }
            }
        });

        findButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MutableLiveData<GeoPointConsulted> newGeoPoint =
                        findLocationViewModel.findLocation(locationText.getText().toString());
                newGeoPoint.observe(getActivity(), new Observer<GeoPointConsulted>() {
                    @Override
                    public void onChanged(GeoPointConsulted geoPointConsulted) {
                        GeoPoint geoPoint = new GeoPoint(geoPointConsulted.getTitle(), geoPointConsulted.getLatitude(), geoPointConsulted.getLongitude());
                        findLocationViewModel.setCenterPoint(geoPoint);
                    }
                });
            }
        });

        findLocationViewModel.getCenterPoint().observe(getActivity(), new Observer<GeoPoint>() {
            @Override
            public void onChanged(GeoPoint geoPoint) {
                findLocationViewModel.findParkingAreas(geoPoint).observe(getActivity(), new Observer<List<ParkingAreaDto>>() {
                    @Override
                    public void onChanged(List<ParkingAreaDto> parkingAreaDtos) {
                        findLocationViewModel.setParkingAreas(parkingAreaDtos);
                    }
                });
            }
        });
        ejecutar();

        return root;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_json)));
        this.googleMap = googleMap;
        findLocationViewModel.getCenterPoint().observe(getActivity(), centerPointObserver);
        findLocationViewModel.getParkingAreas().observe(getActivity(), parkingAreaObserver);
        findLocationViewModel.getParkingSpaces().observe(getActivity(), parkingSpacesObserver);
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                findLocationViewModel.setParkingSpaceNameTarget(marker.getTitle());
                findLocationViewModel.setParkingAreaNameTarget(marker.getSnippet());
                String parkingSpaceId = findLocationViewModel.getParkingSpaceId(marker.getTitle());
                String state = findLocationViewModel.getParkingSpaceState(parkingSpaceId).getValue().getState();
                findLocationViewModel.setTargetState(state);
                findLocationViewModel.setPrice(findLocationViewModel.getPrice(marker.getSnippet()));
                findLocationViewModel.setHorario(findLocationViewModel.getHOrario(marker.getSnippet()));
                if (SensorState.State.RESERVED.equals(state)){
                    iniciarBtn.setVisibility(View.VISIBLE);
                    finalizarBtn.setVisibility(View.VISIBLE);
                }else {
                    if (SensorState.State.FREE.equals(state)) {
                        iniciarBtn.setVisibility(View.VISIBLE);
                        finalizarBtn.setVisibility(View.INVISIBLE);
                    }else if (SensorState.State.BUSSY.equals(state)) {
                        iniciarBtn.setVisibility(View.INVISIBLE);
                        finalizarBtn.setVisibility(View.VISIBLE);
                    }else {
                        iniciarBtn.setVisibility(View.INVISIBLE);
                        finalizarBtn.setVisibility(View.INVISIBLE);
                    }
                }
                return false;
            }
        });
    }

    private void ejecutar(){
        automatico = new Runnable() {
            @Override
            public void run() {
                updateParkingSpacesStates();//llamamos nuestro metodo
                handler.postDelayed(this,5000);//se ejecutara cada 5 segundos
            }
        };
        handler.postDelayed(automatico,5000);//empezara a ejecutarse después de 5 milisegundos
    }

    private void updateParkingSpacesStates(){
        List<LiveData<SensorStateDto>> dataList = findLocationViewModel.findParkingSpacesStates();
        if(dataList != null){
            for (LiveData<SensorStateDto> sensorState: dataList ) {
                sensorState.observe(getActivity(), new Observer<SensorStateDto>() {
                    @Override
                    public void onChanged(SensorStateDto sensorStateDto) {
                        if (sensorStateDto != null) {
                            findLocationViewModel.updateParkingSpaceState(sensorStateDto.getName(), sensorStateDto);
                        }
                    }
                });
            }
        }
    }

    final Observer<List<String>> parkingSpacesObserver = (parkingSpaces)->{
        if(parkingSpaces.size() > 0){
            for (String parkingSpaceId: parkingSpaces ) {
                findLocationViewModel.getParkingSpaceState(parkingSpaceId)
                        .observe(getActivity(), (sensorStateDto) -> {
                            Marker marker = findLocationViewModel.getMarker(parkingSpaceId);
                            String parkingSpaceName = findLocationViewModel.getParkingSpaceName(parkingSpaceId);
                            MarkerOptions newMarker = new MarkerOptions().position(marker.getPosition())
                                    .title(parkingSpaceName).snippet(marker.getSnippet()).anchor(0.5f,0.5f);
                            if (SensorState.State.BUSSY.equals(sensorStateDto.getState())) {
                                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_ocupado));
                            }else if (SensorState.State.FREE.equals(sensorStateDto.getState())) {
                                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_libre));
                            }else if (SensorState.State.RESERVED.equals(sensorStateDto.getState())) {
                                newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor_reservado));
                            }else{
                                return ;
                            }
                            marker.remove();
                            findLocationViewModel.updateMarker(parkingSpaceId,googleMap.addMarker(newMarker));
                        });
            }
        }
    };

    final Observer<GeoPoint> centerPointObserver = (geoPoint)->{
        googleMap.clear();
        LatLng centerPoint = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(centerPoint).title(geoPoint.getTitle()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(centerPoint));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
    };

    final Observer<List<ParkingAreaDto>> parkingAreaObserver = (parkingAreaDtos)->{
        if(parkingAreaDtos != null && parkingAreaDtos.size() > 0){
            Map<String, Marker> parkingSpacesMarkers = new HashMap<>();
            Map<String, String> parkingSpacesNames = new HashMap<>();
            for (ParkingAreaDto parkingArea: parkingAreaDtos ) {
                String parkingAreaName = parkingArea.getName();
                findLocationViewModel.addCLientPrice(parkingAreaName,parkingArea.getPrecio());
                findLocationViewModel.addCLientHorario(parkingAreaName, parkingArea.getDisponibilidad());

                LatLng centerArea = new LatLng(parkingArea.getCenterPoint().getLatitude(), parkingArea.getCenterPoint().getLongitude());
                googleMap.addMarker(new MarkerOptions().position(centerArea).title(parkingArea.getName()).snippet(FIndLocationFragmentAdmin.MarkerType.PARKING_AREA));

                if(parkingArea.getParkingSpaces() != null && parkingArea.getParkingSpaces().size() > 0){
                    for (ParkingSpaceDto parkingSpace: parkingArea.getParkingSpaces() ) {
                        String parkingSpaceName = parkingSpace.getName();
                        String parkingSpaceId = parkingSpace.getId();
                        if (parkingSpace.getSensor() != null
                                && parkingSpace.getSensor().getLatitude() != null
                                && parkingSpace.getSensor().getLatitude() != null){
                            SensorDto sensor = parkingSpace.getSensor();
                            LatLng sensorPoint = new LatLng(sensor.getLatitude(), sensor.getLongitude());
                            MarkerOptions markerOptions = new MarkerOptions().position(sensorPoint)
                                    .title(parkingSpaceName).snippet(parkingAreaName).anchor(0.5f,0.5f)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.sensor));
                            parkingSpacesMarkers.put(parkingSpaceId,googleMap.addMarker(markerOptions));
                            parkingSpacesNames.put(parkingSpaceId,parkingSpaceName);
                        }
                    }
                }
                if(parkingArea.getPolygonPoints()!=null && parkingArea.getPolygonPoints().size() > 0){
                    PolygonOptions options = new PolygonOptions();
                    for (int j = 0; j < parkingArea.getPolygonPoints().size() ; j++) {
                        PolygonPointsDto pointt = parkingArea.getPolygonPoints().get(j);
                        options.add(new LatLng(pointt.getLatitude(), pointt.getLongitude()));
                    }
                    options.strokeColor(0xFF00AA00)
                            .fillColor(0x2200FFFF)
                            .strokeWidth(2);
                    Polygon polygon = googleMap.addPolygon(options);

                }
            }
            findLocationViewModel.setParkingSpaces(parkingSpacesNames, parkingSpacesMarkers);
        }
    };

    final Observer<SensorStateDto> updateParkingSpapceStateObserver = (sensorStateDto) -> {

    };

    View.OnClickListener reserveAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ReservationManager.getInstance().crearReserva(
                    findLocationViewModel.getParkingAreaNameTarget().getValue(),
                    findLocationViewModel.getParkingSpaceNameTarget().getValue(),
                    findLocationViewModel.getParkingSpaceId(findLocationViewModel.getParkingSpaceNameTarget().getValue()),
                    findLocationViewModel.findParkingAreaNumber(findLocationViewModel.getParkingAreaNameTarget().getValue()),
                    findLocationViewModel.getClientPrice().getValue()
            );
            Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.nav_reserve);
        }
    };

    @Override
    public void onDestroyView() {
        handler.removeCallbacks(automatico);
        super.onDestroyView();
    }

    private class CambiarEstado extends AsyncTask<String,Integer,Integer> {

        String jsonRespuestaEnString;

        @Override
        protected void onPreExecute(){

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Integer doInBackground(String... params) {

            /**
             * Convierte la información en DTOUsuario.
             */
            String dto = (params[0]);
            HttpURLConnection conection = null;
            Boolean resutl = false;

            try {
                /**
                 * Define los datos de la conexión y sus caracteristicas, abre la conexión.
                 */
                URL url = new URL(ApiBuilder.MENDOPARK_URL + "sensorstate/fromUser");
                conection = (HttpURLConnection) url.openConnection();

                conection.setRequestMethod("POST");
                conection.setDoInput(true);
                conection.setDoOutput(true);
                conection.setRequestProperty("Content-Type", "text/plain; charset=UTF-8");

                /**
                 * Prepara el envío de datos mediante las clases OutputStream y BufferedWriter,
                 * en este caso el DTOUsuario.
                 */
                OutputStream outputStream = new BufferedOutputStream(conection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
                writer.write(dto);
                writer.flush();
                writer.close();

                /**
                 * Envía el DTOUsuario conectándose
                 */

                conection.setConnectTimeout(1000);
                conection.setReadTimeout(1000);

                try {
                    conection.connect();
                } catch (SocketTimeoutException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                } catch (ConnectException e) {
//                    EstadoApp.getInstancia().setTieneConexion(false);
                    return null;
                }

                /**
                 * Prepara la recepción de datos de la conexión mediante las clases InputStream
                 * y InputStreamReader.
                 */
                InputStream in = new BufferedInputStream(conection.getInputStream());
                InputStreamReader is = new InputStreamReader(in);

                /**
                 * Convierte los datos recibidos en un String y cierra la conexión
                 */
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);

                String read = br.readLine();

                while (read != null) {
                    sb.append(read);
                    read = br.readLine();
                }

                writer.close();
                br.close();
                conection.disconnect();

                jsonRespuestaEnString = sb.toString();

            } catch (MalformedURLException e) {
                Toast.makeText(getContext(), "Error en la URL", Toast.LENGTH_SHORT).show();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return 0;

        }

        /**
         * Después de terminar de ejecutar la comunicación procesa la información recibida.
         * Convierte el String de restpuesta en un DTORespuesta, si es correcto lo direcciona
         * a la venta principal, sino muestra mensaje de error según el detalle del error.
         */
        @Override
        protected void onPostExecute(Integer aVoid) {

        }
    }
}
