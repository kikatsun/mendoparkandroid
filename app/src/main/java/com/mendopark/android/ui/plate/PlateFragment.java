package com.mendopark.android.ui.plate;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.mendopark.android.R;
import com.mendopark.android.dto.plate.VehicleDto;
import com.mendopark.android.dto.plate.VehicleTypeDto;

import java.util.List;
import java.util.ResourceBundle;

public class PlateFragment extends Fragment {

    public PlateViewModel plateViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        plateViewModel =
                ViewModelProviders.of(this, new PlateViewModelFactory()).get(PlateViewModel.class);
        View root = inflater.inflate(R.layout.fragment_plate, container, false);

        final RadioGroup platesRadios = root.findViewById(R.id.radio_group_plates);
        final RadioGroup platesTypesRadios = root.findViewById(R.id.patenteTipos);

        final EditText domain = root.findViewById(R.id.patenteNombre);
        final EditText modelo = root.findViewById(R.id.patenteNombre2);
        final EditText anio = root.findViewById(R.id.patenteNombre3);
        final EditText brand = root.findViewById(R.id.patenteNombre4);
        final EditText color = root.findViewById(R.id.patenteNombre5);

        final Button guardar = root.findViewById(R.id.buttonPatenteGuardar);
        final Button eliminar = root.findViewById(R.id.buttonPatenteEliminar);

        plateViewModel.setVechicle(new VehicleDto());

        plateViewModel.findVehiclesTypes().observe(getActivity(), new Observer<List<VehicleTypeDto>>() {
            @Override
            public void onChanged(List<VehicleTypeDto> vehicleTypeDtos) {
                if(platesTypesRadios != null) {
                    platesTypesRadios.removeAllViews();
                }
                int aa = 5000 ;
                if(vehicleTypeDtos != null && vehicleTypeDtos.size() > 0){
                    for (VehicleTypeDto type: vehicleTypeDtos ) {
                        RadioButton radioButton = new RadioButton(getContext());
                        radioButton.setId(new Integer(aa+type.getNumber()));
                        radioButton.setText(type.getName());
                        radioButton.setTag(type.getNumber());
                        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    plateViewModel.updateSelectVehicleType((Integer)buttonView.getTag(),
                                            domain.getText().toString(),
                                            brand.getText().toString(),
                                            color.getText().toString(),
                                            modelo.getText().toString(),
                                            anio.getText().toString()
                                            );
                                }
                            }
                        });
                        platesTypesRadios.addView(radioButton);
                    }
                }
            }
        });

        plateViewModel.findPayTypes().observe(getActivity(), new Observer<List<VehicleDto>>() {
            @Override
            public void onChanged(List<VehicleDto> vehicleDtos) {
                platesRadios.removeAllViews();
                if(vehicleDtos != null && vehicleDtos.size() > 0){
                    for (VehicleDto vechVehicleDto: vehicleDtos ) {
                        RadioButton radioButton = new RadioButton(getContext());
                        radioButton.setText(vechVehicleDto.getDomain());
                        radioButton.setTag(vechVehicleDto.getDomain());
                        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    plateViewModel.updateSelectVehicle(buttonView.getText().toString());
                                }
                            }
                        });
                        platesRadios.addView(radioButton);
                    }
                }
            }
        });

        plateViewModel.getVechicle().observe(getActivity(), new Observer<VehicleDto>() {
            @Override
            public void onChanged(VehicleDto vehicleDto) {
                if(vehicleDto != null){
                    if(vehicleDto.getDomain() != null){
                        domain.setText(vehicleDto.getDomain());
                    }else{
                        domain.setText("");
                    }
                    if(vehicleDto.getModel() != null){
                        modelo.setText(vehicleDto.getModel());
                    }else{
                        modelo.setText("");
                    }
                    if(vehicleDto.getYear() != null){
                        anio.setText(String.valueOf(vehicleDto.getYear()));
                    }else{
                        anio.setText("");
                    }
                    if(vehicleDto.getBrand() != null){
                        brand.setText(vehicleDto.getBrand());
                    }else{
                        brand.setText("");
                    }
                    if(vehicleDto.getColor() != null){
                        color.setText(vehicleDto.getColor());
                    }else{
                        color.setText("");
                    }
                }else{
                    domain.setText("");
                    modelo.setText("");
                    anio.setText("");
                    brand.setText("");
                    color.setText("");
                    platesTypesRadios.clearCheck();
                }
            }
        });

        plateViewModel.getTypeSelected().observe(getActivity(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                platesTypesRadios.check(new Integer(5000+integer));
            }
        });

        guardar.setOnClickListener(v -> {
            String plate = domain.getText().toString();
            if(plate != null && !plate.isEmpty()) {
                plateViewModel.saveNewPlate(
                        domain.getText().toString(),
                        brand.getText().toString(),
                        color.getText().toString(),
                        modelo.getText().toString(),
                        anio.getText().toString()
                    ).observe(getActivity(), new Observer<VehicleDto>() {
                    @Override
                    public void onChanged(VehicleDto vehicleDto) {
                        if(vehicleDto != null){
                            Navigation.findNavController(getActivity(),R.id.nav_host_fragment).navigate(R.id.nav_plates);
                            plateViewModel.setVechicle(new VehicleDto());
                            platesRadios.clearCheck();
                            Toast.makeText(
                                    getContext(),
                                    "Patente guardado correctamente",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
            }else{
                Toast.makeText(
                        getContext(),
                        "Debe indicar una patente",
                        Toast.LENGTH_SHORT
                ).show();
            }
        });


        eliminar.setOnClickListener(v -> {
            String plate = domain.getText().toString();
            if(plate != null && !plate.isEmpty()) {
                plateViewModel.eliminarPLate(
                        domain.getText().toString()
                ).observe(getActivity(), new Observer<VehicleDto>() {
                    @Override
                    public void onChanged(VehicleDto vehicleDto) {
                        if (vehicleDto != null) {
                            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.nav_plates);
                            Toast.makeText(
                                    getContext(),
                                    "Patente eliminada correctamente",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
            }else{
                Toast.makeText(
                        getContext(),
                        "Debe indicar una patente",
                        Toast.LENGTH_SHORT
                ).show();
            }
        });

        return root;
    }
}
