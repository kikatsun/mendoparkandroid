package com.mendopark.android.ui.findlocation;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.repository.FindLocationRepository;
import com.mendopark.android.ui.reserve.ReserveViewModel;

public class FindLocationViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(FindLocationViewModel.class)){
            return (T) new FindLocationViewModel(FindLocationRepository.getInstance(new FindLocationDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class: "+modelClass);
        }
    }
}
