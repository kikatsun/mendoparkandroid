package com.mendopark.android.ui.reserve;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.dto.find.GeoPointConsulted;
import com.mendopark.android.dto.reserve.ReservaDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.repository.FindLocationRepository;
import com.mendopark.android.repository.ReserveRepository;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.retrofit.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReserveViewModel extends ViewModel {

    private MutableLiveData<ReservaDto> reserve;
    private MutableLiveData<Boolean> patentValidation;

    private ReserveRepository reserveRepository;

    public ReserveViewModel(ReserveRepository reserveRepository) {
        reserve = new MutableLiveData<>();
        patentValidation = new MutableLiveData<>();
        this.reserveRepository = reserveRepository;
    }

    public LiveData<ReservaDto> getReserve() {
        return reserve;
    }

    public void updateReserve (ReservaDto reservaDto) {
        reserve.setValue(reservaDto);
    }

    public LiveData<ReservaDto> reserveAction (ReservaDto reservaDto) {
        return reserveRepository.reserveAction(reservaDto);
    }

    public LiveData<ReservaDto> cancelReserve (ReservaDto reserva) {
        return reserveRepository.cancelAction(reserva);
    }

    public MutableLiveData<Boolean> getPatentValidation() {
        return patentValidation;
    }

    public void setPatentValidation(Boolean patentValidation) {
        this.patentValidation.setValue(patentValidation);
    }

    public boolean validatePatent(String patent) {
        boolean validate = true;
        if(patent.length() == 6){
            char[] chars = patent.toCharArray();
            for(int i=0; i<6; i++){
                char currentChar = chars[i];
                if(i <3 && !Character.isAlphabetic(currentChar)){
                    validate = false;
                    break;
                }else if (i>2 && !Character.isDigit(currentChar)){
                    validate = false;
                    break;
                }
            }
        } else if (patent.length() == 7) {
            char[] chars = patent.toCharArray();
            for(int i=0; i<6; i++){
                char currentChar = chars[i];
                if((i==0 || i == 1) && !Character.isAlphabetic(currentChar)){
                    validate = false;
                    break;
                }else if ((i==2 || i == 3 || i == 4) && !Character.isDigit(currentChar)){
                    validate = false;
                    break;
                } else if ((i==5 || i == 6) && !Character.isDigit(currentChar)){
                    validate = false;
                    break;
                }
            }
        }else {
            validate = false;
        }
        if(validate){
            setPatentValidation(validate);
        }
        return validate;
    }
}