package com.mendopark.android.ui.parking;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mendopark.android.R;
import com.mendopark.android.adapters.ParkingAdapter;
import com.mendopark.android.dto.ParkingModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Parking extends Fragment {

    private ParkingViewModel mViewModel;
    private RecyclerView rv;
    private LinearLayoutManager layoutManager;
    private List<ParkingModel> parkingModelsList;

    public static Parking newInstance() {
        return new Parking();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.parking_fragment, container, false);
        mViewModel = ViewModelProviders.of(this, new ParkingViewModelFactory()).get(ParkingViewModel.class);

        parkingModelsList = new ArrayList<>();
       // parkingModelsList = mViewModel.getFakeData();
        rv = v.findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(getContext());
        rv.setLayoutManager(layoutManager);
        ParkingAdapter parkingAdapter = new ParkingAdapter(parkingModelsList,getContext());
        rv.setAdapter(parkingAdapter);
        mViewModel.findParking().observe(getActivity(), new Observer<List<ParkingModel>>() {
            @Override
            public void onChanged(List<ParkingModel> parkingModels) {
                ParkingAdapter parkingAdapter2 = new ParkingAdapter(parkingModels,getContext());
                rv.setAdapter(parkingAdapter2);
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ParkingViewModel.class);
        // TODO: Use the ViewModel
    }

}
