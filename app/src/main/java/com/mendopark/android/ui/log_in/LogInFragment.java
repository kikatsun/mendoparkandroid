package com.mendopark.android.ui.log_in;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.mendopark.android.MainActivity;
import com.mendopark.android.R;
import com.mendopark.android.dto.MendoParkSessionDto;
import com.mendopark.android.service.Preferencias;
import com.mendopark.android.service.ResponseFactory;

public class LogInFragment extends Fragment {

    private LogInViewModel logInViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        logInViewModel =
                ViewModelProviders.of(this, new LogInViewModelFactory()).get(LogInViewModel.class);
        View root = inflater.inflate(R.layout.fragment_log_in, container, false);

        final Button signInButton = root.findViewById(R.id.register);
        final Button logInButton = root.findViewById(R.id.loginBtn);

        final EditText userName = root.findViewById(R.id.username);
        final EditText password = root.findViewById(R.id.password);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.nav_sign_in);
            }
        });

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logInViewModel.logInAction(userName.getText().toString(), password.getText().toString()).observe(getActivity(), new Observer<MendoParkSessionDto>() {
                    @Override
                    public void onChanged(MendoParkSessionDto sessionDto) {
                        if(sessionDto != null && sessionDto.getSessionId() != null && !sessionDto.getSessionId().isEmpty()) {
                            Preferencias.getInstanciaSinContexto().setSessionId(sessionDto.getSessionId());
                            Preferencias.getInstanciaSinContexto().updateUserRol(sessionDto.getRole());
                            Toast.makeText(
                                    getContext(),
                                    "Las session es: "+sessionDto.getSessionId(),
                                    Toast.LENGTH_SHORT
                            ).show();
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(
                                    getContext(),
                                    "Error en la solicitud: "+ ResponseFactory.getInstance().getMEssage("SESSION"),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
            }
        });

        return root;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        logInViewModel.stopHandler();
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroy() {
        logInViewModel.stopHandler();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        logInViewModel.stopHandler();
        super.onDestroyView();
    }
}
