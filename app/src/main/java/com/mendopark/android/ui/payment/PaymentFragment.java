package com.mendopark.android.ui.payment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mendopark.android.R;
import com.mendopark.android.dto.payment.PagoDto;
import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.model.PaymentType;
import com.mendopark.android.service.payment.PaymentManager;
import com.mendopark.android.service.payment.TarjetManager;
import com.mendopark.android.ui.ViewModelFactory;

import java.util.List;
import java.util.UUID;

public class PaymentFragment extends Fragment {

    private PaymentViewModel paymentViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        paymentViewModel =
                ViewModelProviders.of(this, new PaymentViewModelFactory()).get(PaymentViewModel.class);
        View root = inflater.inflate(R.layout.fragment_payment_all, container, false);

        paymentViewModel.setPago(PaymentManager.getInstance().getNuevoPago());

        final TextView price = root.findViewById(R.id.importe_pago);
        if (paymentViewModel.getPago().getValue() != null) {
            price.setText("$" + paymentViewModel.getPago().getValue().getMonto().toString());
            final RadioGroup payTypesRadios = root.findViewById(R.id.radio_group_payTypes);
            final RadioGroup tarjetsRadios = root.findViewById(R.id.radio_group_tarjets);
            final Button paymentButton = root.findViewById(R.id.pagar_btn);

            paymentViewModel.findPayTypes().observe(getActivity(), new Observer<List<PayTypeDto>>() {
                @Override
                public void onChanged(List<PayTypeDto> payTypeDtos) {
                    if (payTypeDtos != null && payTypeDtos.size() > 0) {
                        for (PayTypeDto payType : payTypeDtos) {
                            RadioButton radioButton = new RadioButton(getContext());
                            radioButton.setText(payType.getName());
                            radioButton.setTag(payType.getNumber());
                            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        paymentViewModel.setPayTypeSelected((Long) buttonView.getTag());
                                    }
                                }
                            });
                            payTypesRadios.addView(radioButton);
                        }
                    }
                }
            });

            paymentViewModel.getPayTypeSelected().observe(getActivity(), new Observer<Long>() {
                @Override
                public void onChanged(Long aLong) {
                    if (aLong != null) {
                        if (aLong.equals(PaymentType.CREDIT_CARD) || aLong.equals(PaymentType.DEBIT_CARD)) {
                            tarjetsRadios.setVisibility(View.VISIBLE);
                            if (tarjetsRadios.getChildCount() == 0) {
                                List<String> tarjets = TarjetManager.getInstance().getCardNumberFinal();
                                if (tarjets != null && tarjets.size() > 0) {
                                    for (String tarjet : tarjets) {
                                        RadioButton radioButton = new RadioButton(getContext());
                                        radioButton.setText(tarjet);
                                        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                            @Override
                                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                if (isChecked) {
                                                    paymentViewModel.setTarjetSelected(buttonView.getText().toString());
                                                }
                                            }
                                        });
                                        tarjetsRadios.addView(radioButton);
                                    }
                                }
                            }
                            paymentButton.setVisibility(View.INVISIBLE);
                        } else if (aLong.equals(PaymentType.CASH)) {
                            paymentButton.setVisibility(View.VISIBLE);
                            tarjetsRadios.setVisibility(View.INVISIBLE);
                        } else {
                            paymentButton.setVisibility(View.VISIBLE);
                            paymentButton.setText("Tipo de pago no disponible");
                            paymentButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(
                                            getContext(),
                                            "No disponible el tipo de pago,\npor favor elija otro",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                }
                            });
                        }
                    }
                }
            });

            paymentViewModel.getTarjetSelected().observe(getActivity(), new Observer<String>() {
                @Override
                public void onChanged(String s) {
                    paymentButton.setVisibility(View.VISIBLE);
                }
            });

            paymentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PagoDto pago = paymentViewModel.getPago().getValue();
                    if (pago != null) {
                        pago.setFechaPago(System.currentTimeMillis());
                        pago.setId(UUID.randomUUID().toString());
                        PaymentManager.getInstance().setNuevoPago(pago);
                        Toast.makeText(
                                getContext(),
                                "Pago registrado con éxito.\n" +
                                    "Nro. Pago: "+pago.getId(),
                                Toast.LENGTH_SHORT
                        ).show();
                    } else {
                        Toast.makeText(
                                getContext(),
                                "Error, no se pudo realizar el pago correctamente",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }
            });
        }
        return root;
    }
}
