package com.mendopark.android.ui.sign_in;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Guarda el estado de validación de cada campo del formulario de Registro de Usuario, junto al id
 * del mensaje de error asociado a cada campo.
 */
public class SignInFormState {

    /* Campos del formulario de Registro de Usuario */
    public enum Field{
        NAME,
        LAST_NAME,
        USER_NAME,
        PASSWORD,
        RE_PASSWORD,
        EMAIL,
        DOCUMENT_NUMBER
    }

    /* Campos inválido junto a sus respectivos mensajes de error */
    private Map<Field,Integer> messageErroByField = new HashMap<>();
    /* Estado general del formulario */
    private boolean isValid;

    SignInFormState (boolean isValid){
        this.isValid = isValid;
    }

    SignInFormState(@NonNull Map<Field, Integer> fields){
        for (Field field: fields.keySet() ) {
            messageErroByField.put(field,fields.get(field));
        }
        isValid=false;
    }

    public boolean isValid(){
        if(messageErroByField.size() == 0){
            isValid = true;
        }
        return isValid;
    }

    public Integer getMessageError(Field field){
        Integer result = null;
        if(messageErroByField.containsKey(field)){
            result = messageErroByField.get(field);
        }
        return result;
    }

}
