package com.mendopark.android.ui.plate;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.FindLocationDataRest;
import com.mendopark.android.repository.FindLocationRepository;

public class PlateViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(PlateViewModel.class)){
            return (T) new PlateViewModel(FindLocationRepository.getInstance(new FindLocationDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class: "+modelClass);
        }
    }
}
