package com.mendopark.android.ui.reserve;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.ReserveDataRest;
import com.mendopark.android.repository.ReserveRepository;

public class ReserveViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ReserveViewModel.class)){
            return (T) new ReserveViewModel(ReserveRepository.getInstance(new ReserveDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class: "+modelClass);
        }
    }
}
