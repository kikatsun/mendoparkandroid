package com.mendopark.android.ui.stepper;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mendopark.android.R;
import com.mendopark.android.model.Role;
import com.mendopark.android.service.Preferencias;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

/**
 * A simple {@link Fragment} subclass.
 */
public class Stepper extends Fragment implements BlockingStep {

    public static String CURRENT_STEP_POSITION_KEY = "currentPosition";

    public Stepper() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_stepper, container, false);
        ImageView imageView = v.findViewById(R.id.imageViewStepper);
        int position = getArguments().getInt(CURRENT_STEP_POSITION_KEY);
        int roleId = Preferencias.getInstanciaSinContexto().getUserRole();

        if (roleId == Role.OPERATIONAL_USER.getRoleId() ||
                roleId == Role.CLIENT_ADMIN_USER.getRoleId() ||
                roleId == Role.SUPERVISOR_USER.getRoleId()){

            switch (position) {
                case 0:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_1));
                    break;
                case 1:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_2));
                    break;
                case 2:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_3));
                    break;
                case 3:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_4));
                    break;
                case 4:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_5));
                    break;
                case 5:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_6));
                    break;
                case 6:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_7));
                    break;
                case 7:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.estacionar_8));
                    break;

            }
        }else {
            switch (position) {
                case 0:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_1));
                    break;
                case 1:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_2));
                    break;
                case 2:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_3));
                    break;
                case 3:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_4));
                    break;
                case 4:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_5));
                    break;
                case 5:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_6));
                    break;
                case 6:
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.reserva_7));
                    break;

            }
        }
        return v;
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
