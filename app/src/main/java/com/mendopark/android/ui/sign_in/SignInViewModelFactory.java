package com.mendopark.android.ui.sign_in;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.SignInDataRest;
import com.mendopark.android.repository.SignInRepository;

public class SignInViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(SignInViewModel.class)){
            return (T) new SignInViewModel(SignInRepository.getInstance(new SignInDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
