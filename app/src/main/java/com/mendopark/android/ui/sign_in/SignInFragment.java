package com.mendopark.android.ui.sign_in;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.mendopark.android.R;
import com.mendopark.android.dto.UserDto;

import java.util.Arrays;
import java.util.List;

public class SignInFragment extends Fragment {

    private SignInViewModel signInViewModel;
    private List<SignInFormState.Field> fieldsNames = Arrays.asList(
            SignInFormState.Field.NAME, SignInFormState.Field.LAST_NAME,
            SignInFormState.Field.USER_NAME, SignInFormState.Field.PASSWORD,
            SignInFormState.Field.RE_PASSWORD, SignInFormState.Field.EMAIL,
            SignInFormState.Field.DOCUMENT_NUMBER);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        signInViewModel =
                ViewModelProviders.of(this, new SignInViewModelFactory()).get(SignInViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sign_in, container, false);

        final EditText name = root.findViewById(R.id.sign_in_name);
        final EditText lastName = root.findViewById(R.id.sign_in_last_name);
        final EditText userName = root.findViewById(R.id.sign_in_user_name);
        final EditText password = root.findViewById(R.id.sign_in_password);
        final EditText rePassword = root.findViewById(R.id.sign_in_re_password);
        final EditText email = root.findViewById(R.id.sign_in_email);
        final EditText documentNumber = root.findViewById(R.id.sign_in_document_number);
        final Button signInBtn = root.findViewById(R.id.sign_in_btn);

        signInViewModel.getSignInFormState().observe(this, new Observer<SignInFormState>() {
            @Override
            public void onChanged(@Nullable SignInFormState signInFormState) {
                if(signInFormState == null){
                    return;
                }
                signInBtn.setEnabled(signInFormState.isValid());
                updateErrorMessage(signInFormState, SignInFormState.Field.NAME, name);
                updateErrorMessage(signInFormState, SignInFormState.Field.LAST_NAME, lastName);
                updateErrorMessage(signInFormState, SignInFormState.Field.USER_NAME, userName);
                updateErrorMessage(signInFormState, SignInFormState.Field.PASSWORD, password);
                updateErrorMessage(signInFormState, SignInFormState.Field.RE_PASSWORD, rePassword);
                updateErrorMessage(signInFormState, SignInFormState.Field.EMAIL, email);
                updateErrorMessage(signInFormState, SignInFormState.Field.DOCUMENT_NUMBER, documentNumber);
            }
        });

        TextWatcher afterSignInFormChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // no actions
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // no actions
            }

            @Override
            public void afterTextChanged(Editable s) {
                signInViewModel.signInDataChange( name.getText().toString(),
                    lastName.getText().toString(), userName.getText().toString(),
                    password.getText().toString(), rePassword.getText().toString(),
                    email.getText().toString(), documentNumber.getText().toString() );
            }
        };

        name.addTextChangedListener(afterSignInFormChangedListener);
        lastName.addTextChangedListener(afterSignInFormChangedListener);
        userName.addTextChangedListener(afterSignInFormChangedListener);
        password.addTextChangedListener(afterSignInFormChangedListener);
        rePassword.addTextChangedListener(afterSignInFormChangedListener);
        email.addTextChangedListener(afterSignInFormChangedListener);
        documentNumber.addTextChangedListener(afterSignInFormChangedListener);

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInViewModel.signIn();
                signInViewModel.getNewUserResult().observe(getActivity(), new Observer<UserDto>() {
                    @Override
                    public void onChanged(UserDto userDto) {
                        if(userDto != null){
                            Toast.makeText(
                                    getContext(),
                                    "El resultado es: "+userDto.getUserName(),
                                    Toast.LENGTH_SHORT
                            ).show();
                        }else{
                            Toast.makeText(
                                    getContext(),
                                    "Error al registrar usuario",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
            }
        });

        return root;
    }

    private void updateErrorMessage(SignInFormState formState, SignInFormState.Field field, EditText editText){
        Integer errorMessageId = formState.getMessageError(field);
        if(errorMessageId != null){
            editText.setError(getString(errorMessageId));
        }
    }
}
