package com.mendopark.android.ui.log_in;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mendopark.android.datasource.rest.LogInDataRest;
import com.mendopark.android.repository.LogInRepository;

public class LogInViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LogInViewModel.class)){
            return (T) new LogInViewModel(LogInRepository.getInstance(new LogInDataRest()));
        }else{
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
