package com.mendopark.android.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitClient {

    private static Retrofit miipRetrofit = null;
    private static Retrofit mendoParkRetrofit = null;
    private static Retrofit openStreetMapRetrofit = null;

    public static Retrofit getMiipRetrofit(String baseUrl) {
        if (miipRetrofit ==null) {
            miipRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return miipRetrofit;
    }

    public static Retrofit getMendoParkRetrofit(String baseUrl) {
        if (mendoParkRetrofit ==null) {
            mendoParkRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return mendoParkRetrofit;
    }

    public static Retrofit getMendoParkRetrofitWrapper(String baseUrl) {
        if (mendoParkRetrofit ==null) {
            mendoParkRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return mendoParkRetrofit;
    }

    public static Retrofit getOpenStreetMapRetrofit(String baseUrl) {
        if (openStreetMapRetrofit ==null) {
            openStreetMapRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return openStreetMapRetrofit;
    }
}
