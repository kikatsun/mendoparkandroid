package com.mendopark.android.retrofit;

import com.mendopark.android.model.SensorState;
import com.mendopark.android.service.rest.geolocalization.OpenStreetMapApi;
import com.mendopark.android.service.rest.gitlab.MiIpAPI;
import com.mendopark.android.service.rest.mendopark.MendoParkApi;
import com.mendopark.android.service.rest.mendopark.ParkingAreaApi;
import com.mendopark.android.service.rest.mendopark.ReserveApi;
import com.mendopark.android.service.rest.mendopark.SensorStateApi;
import com.mendopark.android.service.rest.mendopark.SessionApi;
import com.mendopark.android.service.rest.mendopark.UserApi;

public class ApiBuilder {

    public static final ApiBuilder instance = new ApiBuilder();

    public static final String MI_IP_URL = "http://raw.githubusercontent.com/";
    public static String MENDOPARK_URL = "http://161.35.101.194:8080/mendopark/rest/";
    public static String OPEN_STREET_MAP_URL = "https://nominatim.openstreetmap.org/";
    private static boolean mendoparkUrlCompleted = false;

    private ApiBuilder() {}

    public static ApiBuilder getInstance(){
        return instance;
    }

    public static MiIpAPI getMiIp(){
        return RetrofitClient.getMiipRetrofit(MI_IP_URL).create(MiIpAPI.class);
    }

    public static MendoParkApi getMendoParkApi(){
        return RetrofitClient.getMendoParkRetrofit(MENDOPARK_URL).create(MendoParkApi.class);
    }

    public static UserApi getUserApi(){
        return RetrofitClient.getMendoParkRetrofit(MENDOPARK_URL).create(UserApi.class);
    }

    public static OpenStreetMapApi getOpenStreetMapApi(){
        return RetrofitClient.getOpenStreetMapRetrofit(OPEN_STREET_MAP_URL).create(OpenStreetMapApi.class);
    }

    public static ParkingAreaApi getParkingAreaApi(){
        return RetrofitClient.getMendoParkRetrofit(MENDOPARK_URL).create(ParkingAreaApi.class);
    }

    public static ReserveApi getReserveApi(){
        return RetrofitClient.getMendoParkRetrofit(MENDOPARK_URL).create(ReserveApi.class);
    }

    public static SessionApi getSessionApi(){
        return RetrofitClient.getMendoParkRetrofit(MENDOPARK_URL).create(SessionApi.class);
    }

    public static void updateMendoParkUrl(String ip){
        if(ip != null){
//            MENDOPARK_URL = "http://"+ip+MENDOPARK_URL;
//            mendoparkUrlCompleted = true;
        }
    }

    public static SensorStateApi getSensorStateApi() {
        return RetrofitClient.getMendoParkRetrofitWrapper(MENDOPARK_URL).create(SensorStateApi.class);
    }
}
