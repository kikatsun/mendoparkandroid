package com.mendopark.android.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MiipApi {

    @GET("gterickgt/primero/master/primero.txt")
    Call<String> getMiIp();

}
