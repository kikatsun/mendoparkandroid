package com.mendopark.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mendopark.android.R;
import com.mendopark.android.dto.ParkingModel;
import com.mendopark.android.ui.parking.Parking;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ParkingAdapter extends RecyclerView.Adapter<ParkingAdapter.ParkingAdapterViewHolder> {

    private List<ParkingModel> parkingModels;
    private Context context;
    public ParkingAdapter(List<ParkingModel> parkingModels, Context context) {
        this.context = context;
        this.parkingModels = parkingModels;
    }

    @NonNull
    @Override
    public ParkingAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parking,parent,false);
        return new ParkingAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParkingAdapterViewHolder holder, int position) {
        ParkingModel parkingModel = parkingModels.get(position);
        if (parkingModel.getId() != null){
            holder.id.setText(parkingModel.getId().toString());
        }
        if (parkingModel.getParkingSpaceName() != null){
            holder.name.setText(parkingModel.getParkingSpaceName());
        }
        if (parkingModel.getStart() != null){
            String startDate = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(parkingModel.getStart());
            holder.initial.setText(startDate);
        }
        if (parkingModel.getEnd() != null){
            String endDate = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(parkingModel.getEnd());
            holder.end.setText(endDate);
        }
        holder.hours.setText(String.valueOf(parkingModel.getHours()));
        float priceTotal = (parkingModel.getHours() * parkingModel.getPrice());
        if(priceTotal == 0f){
            priceTotal = parkingModel.getPrice();
        }
        holder.total.setText(String.valueOf( priceTotal ));
        if (parkingModel.getReserva() != null && parkingModel.getReserva().getId() != null) {
            holder.reserveId.setText(parkingModel.getReserva().getId());
            holder.reserveLabel.setVisibility(View.VISIBLE);
            holder.reserveId.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return parkingModels.size();
    }

    static class ParkingAdapterViewHolder extends RecyclerView.ViewHolder{
        TextView id;
        TextView name;
        TextView initial;
        TextView end;
        TextView hours;
        TextView reserveId;
        TextView total;
        TextView reserveLabel;

        public ParkingAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.item_parking_id);
            name= itemView.findViewById(R.id.item_parking_name);
            initial = itemView.findViewById(R.id.item_parking_initial_date);
            end = itemView.findViewById(R.id.item_parking_end_date);
            hours = itemView.findViewById(R.id.item_parking_hours);
            reserveId = itemView.findViewById(R.id.reserva_id_valor);
            reserveLabel = itemView.findViewById(R.id.dereservatext);
            total = itemView.findViewById(R.id.item_parking_total);
        }
    }
}
