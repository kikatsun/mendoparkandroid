package com.mendopark.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.mendopark.android.ActivityMainUser;
import com.mendopark.android.MainActivityAdmin;
import com.mendopark.android.model.Role;
import com.mendopark.android.service.Preferencias;
import com.mendopark.android.ui.stepper.Stepper;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class StepperAdapter extends AbstractFragmentStepAdapter {


    public StepperAdapter(FragmentManager fm, Context context){
        super(fm,context);
    }


    @Override
    public Step createStep(int position) {

        int roleId = Preferencias.getInstanciaSinContexto().getUserRole();
        
        if (roleId == Role.OPERATIONAL_USER.getRoleId() ||
                roleId == Role.CLIENT_ADMIN_USER.getRoleId() ||
                roleId == Role.SUPERVISOR_USER.getRoleId()){

            switch (position) {
                case 0:
                    Bundle b1 = new Bundle();
                    final Stepper step1 = new Stepper();
                    b1.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step1.setArguments(b1);
                    return step1;
                case 1:
                    Bundle b2 = new Bundle();
                    final Stepper step2 = new Stepper();
                    b2.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step2.setArguments(b2);
                    return step2;
                case 2:
                    Bundle b3 = new Bundle();
                    final Stepper step3 = new Stepper();
                    b3.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step3.setArguments(b3);
                    return step3;
                case 3:
                    Bundle b4 = new Bundle();
                    final Stepper step4 = new Stepper();
                    b4.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step4.setArguments(b4);
                    return step4;
                case 4:
                    Bundle b5 = new Bundle();
                    final Stepper step5 = new Stepper();
                    b5.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step5.setArguments(b5);
                    return step5;
                case 5:
                    Bundle b6 = new Bundle();
                    final Stepper step6 = new Stepper();
                    b6.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step6.setArguments(b6);
                    return step6;
                case 6:
                    Bundle b7 = new Bundle();
                    final Stepper step7 = new Stepper();
                    b7.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step7.setArguments(b7);
                    return step7;
                case 7:
                    Bundle b8 = new Bundle();
                    final Stepper step8 = new Stepper();
                    b8.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step8.setArguments(b8);
                    return step8;
            }
        }else {

            switch (position) {
                case 0:
                    Bundle b1 = new Bundle();
                    final Stepper step1 = new Stepper();
                    b1.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step1.setArguments(b1);
                    return step1;
                case 1:
                    Bundle b2 = new Bundle();
                    final Stepper step2 = new Stepper();
                    b2.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step2.setArguments(b2);
                    return step2;
                case 2:
                    Bundle b3 = new Bundle();
                    final Stepper step3 = new Stepper();
                    b3.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step3.setArguments(b3);
                    return step3;
                case 3:
                    Bundle b4 = new Bundle();
                    final Stepper step4 = new Stepper();
                    b4.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step4.setArguments(b4);
                    return step4;
                case 4:
                    Bundle b5 = new Bundle();
                    final Stepper step5 = new Stepper();
                    b5.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step5.setArguments(b5);
                    return step5;
                case 5:
                    Bundle b6 = new Bundle();
                    final Stepper step6 = new Stepper();
                    b6.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step6.setArguments(b6);
                    return step6;
                case 6:
                    Bundle b7 = new Bundle();
                    final Stepper step7 = new Stepper();
                    b7.putInt(Stepper.CURRENT_STEP_POSITION_KEY, position);
                    step7.setArguments(b7);
                    return step7;
            }
        }
        return null;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types

        int roleId = Preferencias.getInstanciaSinContexto().getUserRole();

        if (roleId == Role.OPERATIONAL_USER.getRoleId() ||
                roleId == Role.CLIENT_ADMIN_USER.getRoleId() ||
                roleId == Role.SUPERVISOR_USER.getRoleId()){
            switch (position) {
                case 0:
                    return new StepViewModel.Builder(context)
                            .setTitle("Ingresar a Buscar")
                            .create();
                case 1:
                    return new StepViewModel.Builder(context)
                            .setTitle("Buscar Localizacion")
                            .create();
                case 2:
                    return new StepViewModel.Builder(context)
                            .setTitle("Iniciar Estacionamiento")
                            .create();
                case 3:
                    return new StepViewModel.Builder(context)
                            .setTitle("Plaza ocupada")
                            .create();
                case 4:
                    return new StepViewModel.Builder(context)
                            .setTitle("Finalizar Estacionamiento")
                            .create();
                case 5:
                    return new StepViewModel.Builder(context)
                            .setTitle("Plaza libre")
                            .create();
                case 6:
                    return new StepViewModel.Builder(context)
                            .setTitle("Ir a Estacionamiento")
                            .create();
                case 7:
                    return new StepViewModel.Builder(context)
                            .setTitle("Registro de Estacionamiento")
                            .create();
            }
        }else {
            switch (position) {
                case 0:
                    return new StepViewModel.Builder(context)
                            .setTitle("Ingresar a Buscar")
                            .create();
                case 1:
                    return new StepViewModel.Builder(context)
                            .setTitle("Buscar Localizacion")
                            .create();
                case 2:
                    return new StepViewModel.Builder(context)
                            .setTitle("Selección de Reserva")
                            .create();
                case 3:
                    return new StepViewModel.Builder(context)
                            .setTitle("Nueva Reserva")
                            .create();
                case 4:
                    return new StepViewModel.Builder(context)
                            .setTitle("Pago anticipado")
                            .create();
                case 5:
                    return new StepViewModel.Builder(context)
                            .setTitle("Volver a la Reserva")
                            .create();
                case 6:
                    return new StepViewModel.Builder(context)
                            .setTitle("Último paso de Reserva")
                            .create();
            }
        }

        return null;
    }
    @Override
    public int getCount() {
        int roleId = Preferencias.getInstanciaSinContexto().getUserRole();

        if (roleId == Role.OPERATIONAL_USER.getRoleId() ||
                roleId == Role.CLIENT_ADMIN_USER.getRoleId() ||
                roleId == Role.SUPERVISOR_USER.getRoleId()) {
            return 8;
        }else {
            return 7;
        }
    }


}
