package com.mendopark.android.datasource.rest;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.dto.UserDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.rest.mendopark.UserApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInDataRest {

    private UserApi userApi;

    public SignInDataRest(){
        userApi = ApiBuilder.getUserApi();
    }

    public LiveData<UserDto> signIn(UserDto newUser){
        final MutableLiveData<UserDto> user = new MutableLiveData<>();
        userApi.signIn(newUser).enqueue(new Callback<UserDto>() {
            @Override
            public void onResponse(Call<UserDto> call, Response<UserDto> response) {
                if( 199 < response.code() && response.code() < 300){
                    user.setValue(response.body());
                    Log.i(LogName.getLogName(SignInDataRest.class),response.raw().toString());
                }else{
                    user.setValue(null);
                    Log.e(LogName.getError(SignInDataRest.class),response.raw().toString());
                }
            }

            @Override
            public void onFailure(Call<UserDto> call, Throwable t) {
                Log.e(LogName.getError(SignInDataRest.class),t.getMessage());
                user.setValue(null);
            }
        });
        return user;
    }

}
