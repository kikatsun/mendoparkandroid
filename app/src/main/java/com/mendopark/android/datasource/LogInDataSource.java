package com.mendopark.android.datasource;

import androidx.lifecycle.LiveData;

import com.mendopark.android.dto.MendoParkSessionDto;

public interface LogInDataSource {

    public LiveData<MendoParkSessionDto> createSession (MendoParkSessionDto sessionDto);

}
