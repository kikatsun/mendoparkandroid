package com.mendopark.android.datasource.rest;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.dto.ParkingModel;
import com.mendopark.android.dto.find.GeoPoint;
import com.mendopark.android.dto.find.GeoPointConsulted;
import com.mendopark.android.dto.find.ParkingAreaDto;
import com.mendopark.android.dto.find.Point;
import com.mendopark.android.dto.find.SensorStateDto;
import com.mendopark.android.dto.payment.PayTypeDto;
import com.mendopark.android.dto.plate.VehicleDto;
import com.mendopark.android.dto.plate.VehicleTypeDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.Preferencias;
import com.mendopark.android.service.rest.geolocalization.OpenStreetMapApi;
import com.mendopark.android.service.openstreetmap.ParameterBuilder;
import com.mendopark.android.service.rest.mendopark.ParkingAreaApi;
import com.mendopark.android.service.rest.mendopark.SensorStateApi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindLocationDataRest {

    private OpenStreetMapApi openStreetMapApi;
    private ParkingAreaApi parkingAreaApi;
    private SensorStateApi sensorStateApi;

    public FindLocationDataRest (){
        openStreetMapApi = ApiBuilder.getOpenStreetMapApi();
        parkingAreaApi = ApiBuilder.getParkingAreaApi();
        sensorStateApi = ApiBuilder.getSensorStateApi();
    }

    public MutableLiveData<GeoPointConsulted> findLocation(final String location){
        final MutableLiveData<GeoPointConsulted> geoPoint = new MutableLiveData<>();
        openStreetMapApi.findPosition(ParameterBuilder.generateUrlQuery(location)).enqueue(new Callback<List<GeoPointConsulted>>() {
            @Override
            public void onResponse(Call<List<GeoPointConsulted>> call, Response<List<GeoPointConsulted>> response) {
                if (response.body().size() > 0){
                    geoPoint.setValue(response.body().get(0));
                    Log.i(LogName.getLogName(FindLocationDataRest.class),response.body().toString());
                }else{
                    Log.e(LogName.getError(FindLocationDataRest.class),"Could not find location");
                    geoPoint.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<List<GeoPointConsulted>> call, Throwable t) {
                Log.e(LogName.getError(FindLocationDataRest.class),"Error while send request.",t);
                geoPoint.setValue(null);
            }
        });
        return geoPoint;
    }

    public MutableLiveData<List<ParkingAreaDto>> findParkingAreas(final GeoPoint point){
        final MutableLiveData<List<ParkingAreaDto>> parkingAreas = new MutableLiveData<>();
        String session = Preferencias.getInstanciaSinContexto().getSessionId();
        parkingAreaApi.findParkingAreas(session, new Point(point.getLatitude(),point.getLongitude())).enqueue(new Callback<List<ParkingAreaDto>>() {
            @Override
            public void onResponse(Call<List<ParkingAreaDto>> call, Response<List<ParkingAreaDto>> response) {
                if (response.body() != null && response.body().size() > 0){
                    parkingAreas.setValue(response.body());
                    Log.i(LogName.getLogName(FindLocationDataRest.class),"There are "+response.body().size()+" parking areas");
                }else{
                    Log.e(LogName.getError(FindLocationDataRest.class),"Could not find parking areas near "+point.toString());
                    parkingAreas.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<List<ParkingAreaDto>> call, Throwable t) {
                Log.e(LogName.getError(FindLocationDataRest.class),"Error while send request.",t);
                parkingAreas.setValue(null);
            }
        });
        return parkingAreas;
    }

    public MutableLiveData<SensorStateDto> findParkingSpaceState(final String parkingSpaceId){
        final MutableLiveData<SensorStateDto> sensorState = new MutableLiveData<>();
        parkingAreaApi.findParkingSpaceState(parkingSpaceId).enqueue(new Callback<SensorStateDto>() {
            @Override
            public void onResponse(Call<SensorStateDto> call, Response<SensorStateDto> response) {
                if (response.code() > 199 && response.code() < 300) {
                    sensorState.setValue(response.body());
                    Log.i(LogName.getLogName(FindLocationDataRest.class), parkingSpaceId + " -> " + response.body());
                } else {
                    sensorState.setValue(null);
                    Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for "+ parkingSpaceId);
                }
            }

            @Override
            public void onFailure(Call<SensorStateDto> call, Throwable t) {
                sensorState.setValue(null);
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for "+ parkingSpaceId);
            }
        });
        return sensorState;
    }

    public MutableLiveData<List<PayTypeDto>> findPayTypes() {
        final MutableLiveData<List<PayTypeDto>> result = new MutableLiveData<>();
        parkingAreaApi.getPayTypes().enqueue(new Callback<List<PayTypeDto>>() {
            @Override
            public void onResponse(Call<List<PayTypeDto>> call, Response<List<PayTypeDto>> response) {
                if (response.code() > 199 && response.code() < 300) {
                    result.setValue(response.body());
                } else {
                    result.setValue(null);
                    Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
                }
            }

            @Override
            public void onFailure(Call<List<PayTypeDto>> call, Throwable t) {
                result.setValue(null);
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });


        return result;
    }

    public MutableLiveData<List<ParkingModel>> findParking() {
        final MutableLiveData<List<ParkingModel>> result = new MutableLiveData<>();
        parkingAreaApi.getParkingModel().enqueue(new Callback<List<ParkingModel>>() {
            @Override
            public void onResponse(Call<List<ParkingModel>> call, Response<List<ParkingModel>> response) {
                if (response.code() > 199 && response.code() < 300) {
                    result.setValue(response.body());
                } else {
                    ParkingModel parkingModel = new ParkingModel();
                    parkingModel.setParkingSpaceName("No hay conexion a Internet");
                    List<ParkingModel> parkingModels = new ArrayList<>();
                    parkingModels.add(parkingModel);
                    result.setValue(parkingModels);
                }
            }

            @Override
            public void onFailure(Call<List<ParkingModel>> call, Throwable t) {
                ParkingModel parkingModel = new ParkingModel();
                parkingModel.setParkingSpaceName("No hay conexion a Internet");
                List<ParkingModel> parkingModels = new ArrayList<>();
                parkingModels.add(parkingModel);
                result.setValue(parkingModels);
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });

        return result;
    }

    public MutableLiveData<List<VehicleDto>> findVechicles() {
        final MutableLiveData<List<VehicleDto>> result = new MutableLiveData<>();
        parkingAreaApi.getVechicles(Preferencias.getInstanciaSinContexto().getSessionId()).enqueue(new Callback<List<VehicleDto>>() {
            @Override
            public void onResponse(Call<List<VehicleDto>> call, Response<List<VehicleDto>> response) {
                if (response.code() > 199 && response.code() < 300) {
                    result.setValue(response.body());
                } else {
                    result.setValue(new ArrayList<>());
                }
            }

            @Override
            public void onFailure(Call<List<VehicleDto>> call, Throwable t) {
                result.setValue(new ArrayList<>());
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });

        return result;
    }

    public MutableLiveData<List<VehicleTypeDto>> findVehiclesTypes() {
        final MutableLiveData<List<VehicleTypeDto>> result = new MutableLiveData<>();
        parkingAreaApi.getVechiclesTypes().enqueue(new Callback<List<VehicleTypeDto>>() {
            @Override
            public void onResponse(Call<List<VehicleTypeDto>> call, Response<List<VehicleTypeDto>> response) {
                if (response.code() > 199 && response.code() < 300) {
                    result.setValue(response.body());
                } else {
                    result.setValue(new ArrayList<>());
                }
            }

            @Override
            public void onFailure(Call<List<VehicleTypeDto>> call, Throwable t) {
                result.setValue(new ArrayList<>());
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });

        return result;
    }

    public MutableLiveData<VehicleDto> savePlate(VehicleDto value) {
        final MutableLiveData<VehicleDto> result = new MutableLiveData<>();
        System.out.println("ERICKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK: se da de alta patente "+value.getDomain());
        parkingAreaApi.save(Preferencias.getInstanciaSinContexto().getSessionId(), value).enqueue(new Callback<VehicleDto>() {
            @Override
            public void onResponse(Call<VehicleDto> call, Response<VehicleDto> response) {
                if (response.code() > 199 && response.code() < 300) {
                    System.out.println("ERICKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK: se da de alta patente OK");
                    result.setValue(response.body());
                } else {
                    System.out.println("ERICKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK: se da de alta patente FAIL");
                    result.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<VehicleDto> call, Throwable t) {
                result.setValue(null);
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });

        return result;
    }

    public MutableLiveData<VehicleDto> eliminarPlate(VehicleDto value) {
        final MutableLiveData<VehicleDto> result = new MutableLiveData<>();
        parkingAreaApi.deletePlate(Preferencias.getInstanciaSinContexto().getSessionId(), value).enqueue(new Callback<VehicleDto>() {
            @Override
            public void onResponse(Call<VehicleDto> call, Response<VehicleDto> response) {
                if (response.code() > 199 && response.code() < 300) {
                    result.setValue(response.body());
                } else {
                    result.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<VehicleDto> call, Throwable t) {
                result.setValue(null);
                Log.e(LogName.getError(FindLocationDataRest.class), new Date().toString() + ": Could not find parking space state for paytypes");
            }
        });

        return result;
    }
}
