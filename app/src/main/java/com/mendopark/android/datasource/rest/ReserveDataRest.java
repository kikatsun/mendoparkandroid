package com.mendopark.android.datasource.rest;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.dto.reserve.ReservaDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.model.ReserveState;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.rest.mendopark.ReserveApi;
import com.mendopark.android.service.rest.mendopark.SensorStateApi;

import java.util.Date;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReserveDataRest {

    private ReserveApi reserveApi;
    private SensorStateApi sensorStateApi;

    public ReserveDataRest() {
        reserveApi = ApiBuilder.getReserveApi();
        sensorStateApi = ApiBuilder.getSensorStateApi();
    }

    public MutableLiveData<ReservaDto> reserveAction(final ReservaDto reservaDto){
        final MutableLiveData<ReservaDto> reserve = new MutableLiveData<>();
        reserveApi.createReserve(reservaDto).enqueue(new Callback<ReservaDto>() {
            @Override
            public void onResponse(Call<ReservaDto> call, Response<ReservaDto> response) {
                if (response.code() > 199 && response.code() < 300) {
                    if (response.body().getId() == null) {
                        response.body().setId(UUID.randomUUID().toString());
                    }
                    reserve.setValue(response.body());
                    Log.i(LogName.getLogName(FindLocationDataRest.class), reservaDto + " -> " + response.body());
                } else {
                    reserve.setValue(null);
                    Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not create reserve "+ reservaDto);
                }
            }

            @Override
            public void onFailure(Call<ReservaDto> call, Throwable t) {
                reserve.setValue(null);
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not create reserve "+ reservaDto);
            }
        });
        return reserve;
    }

    public LiveData<ReservaDto> cancelAction(ReservaDto reserva) {
        final MutableLiveData<ReservaDto> reserve = new MutableLiveData<>();
        sensorStateApi.changeState(reserva.getParkingSpaceName()+"|LIBRE").enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() > 199 && response.code() < 300) {
                    Log.i(LogName.getLogName(FindLocationDataRest.class), "ola");
                } else {
                    Log.e(LogName.getError(FindLocationDataRest.class),"no malo malo");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(LogName.getError(FindLocationDataRest.class),new Date().toString() + ": Could not create reserve "+ reserva);
            }
        });
        return reserve;
    }
}
