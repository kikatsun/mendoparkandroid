package com.mendopark.android.datasource.rest;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mendopark.android.datasource.LogInDataSource;
import com.mendopark.android.dto.MendoParkSessionDto;
import com.mendopark.android.log.LogName;
import com.mendopark.android.retrofit.ApiBuilder;
import com.mendopark.android.service.ResponseFactory;
import com.mendopark.android.service.rest.mendopark.SessionApi;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInDataRest implements LogInDataSource {

    private SessionApi sessionApi;

    public LogInDataRest() {
        sessionApi = ApiBuilder.getSessionApi();
    }

    @Override
    public LiveData<MendoParkSessionDto> createSession (MendoParkSessionDto sessionDto){
        final MutableLiveData<MendoParkSessionDto> session = new MutableLiveData<>();
        sessionApi.createSession(sessionDto).enqueue(new Callback<MendoParkSessionDto>() {
            @Override
            public void onResponse(Call<MendoParkSessionDto> call, Response<MendoParkSessionDto> response) {
                if( 199 < response.code() && response.code() < 300){
                    session.setValue(response.body());
                    Log.i(LogName.getLogName(LogInDataRest.class),response.raw().toString());
                }else{
                    String mess = null;
                    try {
                        mess = response.errorBody().string();
                        mess = mess.substring(
                            mess.indexOf("message\":\"")+10
                        );
                        mess = mess.substring(
                            0,
                            mess.indexOf("\"}")
                        );
                    } catch (IOException e) {
                        mess = "Error en en la lectura de datos";
                    }
                    ResponseFactory.getInstance().addMessage("SESSION", mess);
                    session.setValue(null);
                    Log.e(LogName.getError(LogInDataRest.class),mess);
                }
            }

            @Override
            public void onFailure(Call<MendoParkSessionDto> call, Throwable t) {
                Log.e(LogName.getError(LogInDataRest.class),t.getMessage());
                session.setValue(null);
            }
        });
        return session;
    }

}
